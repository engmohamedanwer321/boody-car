import { AppRegistry,YellowBox,I18nManager } from 'react-native';
import App from './src/App';
I18nManager.allowRTL(false);

YellowBox.ignoreWarnings(['Warning: ...']);
console.disableYellowBox = true;
AppRegistry.registerComponent('CookApplication', () => App);
