import { Navigation } from "react-native-navigation" ;
import { registerScreens } from './screens';

registerScreens() ;

Navigation.startSingleScreenApp({
    screen: {
      screen: 'SplashScreen',
      navigatorStyle:{
        navBarHidden:true
      }
    //   title: 'SignUp', 
    },
    drawer:{
      right: { 
        screen: 'MenuContent'
      },
      left:{
        screen: 'MenuContent'
      }
    },
});