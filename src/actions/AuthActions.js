import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { LOGIN_SUCCESS, LOGIN_FAIL, LOGIN_REQUEST, CHECK_USER_TYPE,
   CURRENT_USER,USER_TOKEN } from './types';
import { LOGIN, BASE_END_POINT } from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import Strigs from '../assets/strings';


export function login(phone, password, FB_token, navigator, moving) {
  return (dispatch,getState) => {    
    dispatch({ type: LOGIN_REQUEST });
    axios.post(LOGIN, JSON.stringify({
        phone: phone,
        password: password,
      }), {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      axios.post(`${BASE_END_POINT}addToken`, JSON.stringify({
        token: FB_token 
      }), {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${res.data.token}`
      },
    }).then(response => {
      console.log("5ي5ي5ي55ي5يي5");
      console.log(res.data);
     
      AsyncStorage.setItem('@BoodyCarUser', JSON.stringify(res.data));  
      dispatch({ type: LOGIN_SUCCESS, payload: res.data});  
      if (res.data.user.type === "CLIENT") {
        //console.log(response.data);
        console.log('oks')
        navigator.resetTo({
          screen: 'CustomerHome',
          animated: true,
        });    
      } else {
        if (res.data.user.active) {
          navigator.resetTo({
            screen: 'OwnerHome',
            animated: true
          })
        } else {
          if(moving){
            navigator.resetTo({
              screen: 'Wait',
              animated: true,
              passProps:{password:password}
          });
          }else{
            RNToasty.Info({ title: Strigs.waitAccept })
          }        
         
        }
      }
    }).catch(error => {
      console.log('inner');
        console.log(error);
      if (!error.response) {
        dispatch({
          type: LOGIN_FAIL,
          payload: Strigs.noConnection,
        });
      } 
    });
    
    })
      .catch(error => {
        if(getState().auth.user){

          return
        }
        console.log('outer');
        console.log(error.response);
        if (!error.response) {
          dispatch({
            type: LOGIN_FAIL,
            payload: Strigs.noConnection,
          });
        } else if (error.response.status == 401) {
          dispatch({
            type: LOGIN_FAIL,
            payload: Strigs.loginError,
          });
        }
      });
  };
}

export function CheckLoginWhenOpen(user){
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}

export function userToken(token){
  return dispatch => {
    dispatch({type:USER_TOKEN,payload:token})
  }
}

