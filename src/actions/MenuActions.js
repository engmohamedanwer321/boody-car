
import { SELECT_MENU } from "./types";



export default function selectMenu(item) {
    return dispatch => {
        dispatch({type: SELECT_MENU, payload: item})
    }
}