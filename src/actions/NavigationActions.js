import { SET_NAVIGATION_SCREEN_NAME } from './types';

export function setNavigationScreenName(name) {
  return async (dispatch, getState) => {
    dispatch({ type: SET_NAVIGATION_SCREEN_NAME, payload: name });
  };
}
