import axios from 'axios';
import Strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import {
    GET_TOP_PRODUCT,GET_TOP_PRODUCT_SUCCESS,GET_TOP_PRODUCT_FAILD,GET_OWNER_PRODUCTS,
    FETCH_PRODUCT_REFRESH,FETCH_PRODUCT_REQUEST,FETCH_PRODUCT_SUCCESS,FETCH_PRODUCT_FAIL
} from './types';


export function getTopProducts() {
    return dispatch => {
        dispatch({type:GET_TOP_PRODUCT});
        axios.get(`${BASE_END_POINT}products/topProduct`)
        .then(response=>{
            dispatch({type:GET_TOP_PRODUCT_SUCCESS,payload:response.data.data})
        }).catch(error=>{
            if (!error.response) {
                dispatch({type:GET_TOP_PRODUCT,payload:Strigs.noConnection})
              }
        })
    }
}


export function getCompanyProducts(companyID,page,refresh) {
    console.log('logologlgogoglgogologlogloglogloglog')
    return dispatch => {
        let uri=`${BASE_END_POINT}products/companies/${companyID}/products?page=${page}&limit=10`
        if(refresh){
            dispatch({type:FETCH_PRODUCT_REFRESH});
        }else{
            dispatch({type:FETCH_PRODUCT_REQUEST});
        }
        axios.get(uri)
        .then(response=>{
            console.log('8888888888')
            console.log(response.data);
            dispatch({type:FETCH_PRODUCT_SUCCESS,payload:response.data.data,pages:response.data.pageCount})
        }).catch(error=>{
            console.log(error.response);
            if (!error.response) {
                dispatch({type:FETCH_PRODUCT_FAIL,payload:Strigs.noConnection})
              }
        })
    }
}

export function searchProducts(search,page,refresh) {
    console.log('logologlgogoglgogologlogloglogloglog')
    return dispatch => {
        let uri=`${BASE_END_POINT}products/categories/1/products?page=${page}&limit=10`
        if(refresh){
            dispatch({type:FETCH_PRODUCT_REFRESH});
        }else{
            dispatch({type:FETCH_PRODUCT_REQUEST});
        }
        axios.get(uri)
        .then(response=>{        
            console.log('8888888888')
            console.log(response.data);
            dispatch({type:FETCH_PRODUCT_SUCCESS,payload:response.data.data,pages:response.data.pageCount})
            if(response.data.data.length==0){
                RNToasty.Info({title:Strings.notResults})
            }
        }).catch(error=>{
            console.log(error.response);
            if (!error.response) {
                dispatch({type:FETCH_PRODUCT_FAIL,payload:Strigs.noConnection})
              }
        })
    }
}

export function searchProducts2(partName,page,refresh) {
    return dispatch => {
        let uri=`${BASE_END_POINT}search/normal?page=${page}&limit=10`
        if(refresh){
            dispatch({type:FETCH_PRODUCT_REFRESH});
        }else{
            dispatch({type:FETCH_PRODUCT_REQUEST});
        }
        axios.post(uri, JSON.stringify({search:partName }), {
            headers: {
              'Content-Type': 'application/json',
            },
          })
        .then(response=>{
            console.log('8888888888')
            console.log(response.data);
            dispatch({type:FETCH_PRODUCT_SUCCESS,payload:response.data.data,pages:response.data.pageCount})
        }).catch(error=>{
            console.log(error.response);
            if (!error.response) {
                dispatch({type:FETCH_PRODUCT_FAIL,payload:Strigs.noConnection})
              }
        })
    }
}

export function getOwnerProducts(ownerID) {
    return dispatch =>{
    axios.get(`${BASE_END_POINT}products/${ownerID}/owner`)
    .then(response=>{
        console.log('88888888888')
        console.log("id    "+response.data.data[0].id )
        dispatch({type:GET_OWNER_PRODUCTS,payload:response.data.data, selectedProduct: response.data.data[0].id,})
       
    })
    .catch(error=>{
        console.log(error)
        if(!error.response){
            dispatch({title:GET_OWNER_PRODUCTS_FAILD, payload: Strings.noConnection})
        }
    })
}
}