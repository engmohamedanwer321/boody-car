import { SELECTED_IMAGES,REMOVE_IMAGE,REMOVE_ALL_IMAGE } from './types';

export function selectedImages(images) {
    return (dispatch) => {
        dispatch({
            type: SELECTED_IMAGES,
            payload: images,
        })
    }
}
export function deleteImage(img) {
    return (dispatch) => {
        dispatch({
            type: REMOVE_IMAGE,
            payload: img,
        })
    }
}

export function removeAllImages(){
    return (dispatch) => {
        dispatch({
            type: REMOVE_ALL_IMAGE,
        })
    }    
}

