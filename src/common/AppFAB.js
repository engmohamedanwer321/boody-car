import React, { Component } from 'react';
import { TouchableOpacity, TouchableWithoutFeedback } from "react-native";
import { Icon, Button } from "native-base";

const AppFAB = ({ iconName, onPress, bottom, disabled, color }) => (

    <TouchableOpacity style={{ ...styles.fab, backgroundColor: color ? color : 'black', bottom: bottom ? bottom : 20, opacity: (disabled ? 0.3 : 1) }} disabled={disabled} onPress={onPress}>
        <Icon style={styles.icon} name={iconName} />
    </TouchableOpacity>
);

const styles = {
    fab: {
        width: 60,
        height: 60,
        borderRadius: 30,
        position: 'absolute',
        left: 15,
        bottom: 20,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "black"
    },
    icon: {
        color: "#5A1879",
        fontSize: 35
    }
}

export default AppFAB;