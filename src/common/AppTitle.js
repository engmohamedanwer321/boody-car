import React, { Component } from "react";
import { Text,Platform } from "react-native";


const AppTitle = ({ text }) => {
    return (
        <Text style={{ ...styles.text }}>{text}</Text>
    )
}


const styles = {

    text: {
        fontFamily: Platform.OS == "ios" ? "Droid Arabic Kufi" : "droidkufi",
        color: "#fff",
        fontSize: Platform.OS == "android" ? 20 : 20,
        textAlign: "center",
        writingDirection:"rtl",
        // fontWeight:"bold"
    }
}

export default AppTitle;