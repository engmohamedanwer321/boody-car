import React, { Component } from 'react';
import {
  View,
  Text,Alert,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Button, Content, Icon } from 'native-base';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/MenuActions';
import logout from "../actions/LogoutActions";
import { AppText, MenuItem } from '../common';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import { rootNavigator } from '../screens/Login';
import Strings from '../assets/strings';
import selectMenu from '../actions/MenuActions';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import LogoutOverlay from '../components/LogoutOverlay';




class MenuContent extends Component {

  state = {
    v: false,
  }

  render() {
    const {item,currentUser} = this.props;
    if(this.props.isRTL){
      Strings.setLanguage('ar')
    }else{
      Strings.setLanguage('en')
    }
    console.log("menu Content item =>   "+item);
    return (
      <View style={styles.container}>
        <View style={{justifyContent:'center',alignItems:'center', backgroundColor:'#FAFAFA',width:responsiveWidth(80),height:responsiveHeight(22)}}>
          <Image
            source={require('../assets/imgs/bluelogo.png')}
            style={{width:responsiveWidth(70),height:responsiveHeight(9)}} />
        </View> 

          {this.props.currentUser?
            <View style={{alignSelf:this.props.isRTL?'flex-end':"flex-start",marginHorizontal:moderateScale(5)}}>
              <AppText textAlign={this.props.isRTL?'right':'left'} fontSize={responsiveFontSize(3)} fontWeight='800'  color={colors.primaryColor} text={currentUser&&currentUser.user.firstname+"  "+currentUser.user.lastname} />
              <AppText textAlign={this.props.isRTL?'right':'left'} text={currentUser&&currentUser.user.email} />
            </View>
            :
            <View style={{width:responsiveWidth(80)}}>
              <Button
              onPress={()=>{
                this.props.navigator.push({
                  screen: 'UserType',
                  animated:true,
                })
              }}
               transparent style={{alignSelf:'center', marginTop:moderateScale(5), justifyContent:'center',alignItems:'center',width:responsiveWidth(40),borderWidth:3,borderColor:colors.primaryColor,borderRadius:moderateScale(5)}}>
                <AppText color={colors.primaryColor} fontSize={responsiveFontSize(3)} text={Strings.signup}/>
              </Button>
            </View>
          }
        
      
        <ScrollView 
        showsVerticalScrollIndicator={false}
        style={{marginTop:moderateScale(20)}}>
          <View style={{ marginBottom: moderateScale(15)}}>
          <MenuItem
              onPress={()=>{
                console.log("main 1 =>   "+item);
                if(item == "MAIN"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('MAIN');
                  this.props.navigator.push({
                    screen:'CustomerHome',
                    animated: true
                  })
                }
              }}
             focused={item == 'MAIN'} iconName='home' text={Strings.home}
             />

              <MenuItem
              onPress={()=>{
                console.log("addvers 1 =>   "+item);
                if(item == 'ADVERTISEMENT'){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('ADVERTISEMENT');
                this.props.navigator.push({
                  screen:'CustomerAdvertisement',
                  animated: true
                })
                }
                console.log("addvers 2 =>   "+item);
              }}
             focused={item == 'ADVERTISEMENT'} iconName='diagnoses' text={Strings.advertisement}
             />

             
            <MenuItem
             onPress={()=>{
             if(this.props.currentUser){
                if(item=="NOTIFICATIONS"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('NOTIFICATIONS');
                  this.props.navigator.push({
                    screen:'Notifications',
                    animated:true,
                  })
                }
             }else{
              RNToasty.Warn({title:Strings.checkUser})
             }
              
            }}
            focused={item == 'NOTIFICATIONS'}
             iconName='bell' text={Strings.notifications}/>

            <MenuItem
            onPress={()=>{
              if(item=="COMPANY"){
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              }else{
                this.props.navigator.push({
                  screen: 'CustomerCompanies',
                  animated: true
                });
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('COMPANY');
              }
            }}
            focused={item == 'COMPANY'}
             iconName='car' text={Strings.company}/>

            <MenuItem
            onPress={()=>{
             
              if(this.props.currentUser){
                if(item=="PROFILE"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                  this.props.selectMenu('PROFILE');
                  this.props.navigator.push({
                    screen:'CustomerProfile',
                    animated:true,
                  })
                }              
              }else{
                  RNToasty.Warn({title:Strings.checkUser})
              }
             
            }}
            focused={item == 'PROFILE'}
             iconName='user' text={Strings.profile}/>

            <MenuItem
            onPress={()=>{
              if(item=="LANGUAGE"){
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              }else{
                this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('LANGUAGE');
                this.props.navigator.push({
                  screen: 'SelectLanguage',
                  animated: true
                }); 
              }
              }}
            focused={item == 'LANGUAGE'}
             iconName='language' text={Strings.languauge}/>

            <MenuItem
            onPress={()=>{
             if(this.props.currentUser){
                if(item=="MYCART"){
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                }else{
                  this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
                this.props.selectMenu('MYCART');
                this.props.navigator.push({
                  screen:'CustomerShoppingBacket',
                  animated:true,
                })
                }
             }else{
              RNToasty.Warn({title:strings.checkUser}) 
             }
            }}
            focused={item == 'MYCART'}
             iconName='shopping-cart' text={Strings.myCart}/>

            {this.props.currentUser&&
            <MenuItem
            onPress={()=>{
              this.props.navigator.toggleDrawer({ side: this.props.isRTL ? 'right' : 'left' });
              this.props.selectMenu('LOGOUT');
              Alert.alert(
                `${Strings.logoutTitle}`,
                `${Strings.logoutMessage}`,
                [
                  {text: `${strings.no}`},
                  {text: `${strings.yes}`, onPress: () =>{
                    this.props.logout(this.props.userToken,this.props.currentUser.token ,this.props.navigator)
                  } },
                ],
              )
            }}
            focused={item == 'LOGOUT'}
             iconName='sign-out-alt' text={Strings.logOut}/>
            }
            
          </View>
        </ScrollView>
       
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
    width: responsiveWidth(82),
    

  },

  linksContainer: {
    flex: 1,
    marginBottom: moderateScale(15),
  },
  linksContainerScroll: {
    flex: 1,
    marginTop: moderateScale(50)
  },

};

const mapStateToProps = state => ({
  item: state.menu.item,
  isRTL: state.lang.RTL,
  currentUser : state.auth.currentUser,
  userToken: state.auth.userToken,

});

const mapDispatchToProps = {
  selectMenu,
  logout,
}

export default connect(mapStateToProps, mapDispatchToProps, )(MenuContent);
