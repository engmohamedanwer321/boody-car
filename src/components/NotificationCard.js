import React,{Component} from 'react';
import {View,Alert,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppText from '../common/AppText';
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);




class NotificationCard extends Component {
    
    state = {
        ground: 0,
    }
    
    componentDidMount(){
        moment.locale(this.props.isRTL ? 'ar' : 'en');
    }

    readNotification = (notID) => {
          console.log(notID)
        axios.put(`${BASE_END_POINT}notif/${notID}/read`,{}, {
            headers: {
              'Content-Type': 'application/json',
              //this.props.currentUser.token
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
        }).then(Response=>{
            console.log(Response)
        }).catch(error=>{
            console.log(error.response)
        })
    }
    render(){
        
        const {active,date,right,notID,notDescription,subject,navigator} = this.props;
        //const dateToFormat = () => <Moment date={date} />,
        return(
            <MyTouchableOpacity style={{
             alignSelf:'center',      
            justifyContent:'center',
            width: responsiveWidth(98),
            backgroundColor:active||this.state.ground>0? 'rgba(250,250,250,0.8)' : '#E8ECFF',
            elevation:2,
            shadowOffset:{width:1,height:2},
            }}
            onPress={()=>{
                this.setState({ground:1});
                this.readNotification(notID)
                if(                   
                    notDescription.toLowerCase().includes('new order'.toLocaleLowerCase())
                    //notDescription.toLowerCase().includes('reply on your search order')
                    ){
                    navigator.push({
                        screen: 'NotificationOrderDetails',
                        animated: true,
                        passProps:{
                            source: 'orders',
                            subject: subject,
                        }
                    })
                    
                }else if(             
                    notDescription.toLowerCase().includes('accept your Product'.toLocaleLowerCase())|| 
                    notDescription.toLowerCase().includes('Product Top'.toLocaleLowerCase())||
                    notDescription.toLowerCase().includes('Product Low'.toLocaleLowerCase())
                    ){
                    navigator.push({
                        screen: 'NotificationProductDetails',
                        animated: true,
                        passProps:{
                            source: 'orders',
                            subject: subject,
                        }
                    })
                }else if(notDescription.toLowerCase().includes('accept your rigister'.toLocaleLowerCase())){
                    navigator.push({
                        screen: 'Accept',
                        animated: true,
                    })
                }else if(notDescription.toLowerCase().includes('someone search about this product'.toLocaleLowerCase())){
                    navigator.push({
                        screen: 'NotificationSearchedProductResponse',
                        animated: true,
                        passProps:{
                            subject: subject,
                        }
                    })
                }               
            }}
            >
                <View style={{marginTop:moderateScale(2)}}>
                    <AppText paddingHorizontal={moderateScale(6)} text ={right?'من مشرف بودى كار':'From Boody Car Admin'} color={colors.primaryColor} fontSize={responsiveFontSize(2.8)} />
                    <AppText paddingHorizontal={moderateScale(10)} text ={notDescription} color='gray' fontSize={responsiveFontSize(2.5)} />
                </View>

                <View style={{marginTop:moderateScale(2),marginBottom:moderateScale(2),alignSelf:right?'flex-start':'flex-end'}}>
                    <AppText paddingHorizontal={moderateScale(6)} color='#505050' text={moment(date).fromNow()} fontSize={responsiveFontSize(2)} />
                </View>

            </MyTouchableOpacity>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});



export default connect(mapStateToProps)(NotificationCard);
