import React,{Component} from 'react';
import {View,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import {Button} from 'native-base';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);

 class NotificationOrderDetailsCard extends Component {
     state={
         basketColor:null,
         added: false,
     }

     
    render(){
        const {data,isRTL,onPress,quantity} = this.props;
        return(
            <MyButton 
            onPress={()=>{
                if(onPress){
                   onPress();
                }
            }}
            style={styles.card}
            >
                <View style={styles.upperView}>
                    <FastImage  style={styles.img} source={{uri:data.img}} />
                </View>

                <View style={styles.centeralView}>
                    <View style={{justifyContent:'center',alignItems:'center',width:responsiveWidth(48)}}>
                        <AppText fontWeight='400' color={colors.primaryColor} fontSize={responsiveFontSize(3)} text={data.name} />
                    </View>
                   <View style={{marginTop:moderateScale(2), marginHorizontal:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',alignSelf:isRTL?'flex-end':'flex-start'}}>
                        <AppText   color={colors.skipIconColor} fontSize={responsiveFontSize(2)} text={Strings.quantity} />
                        <AppText  color='gray' fontSize={responsiveFontSize(2)} text={`  ${quantity}`} />
                   </View>
                </View>

                <View style={styles.lowerView}>
                    <View style={{marginHorizontal:moderateScale(5),alignItems:'center', flex:1,flexDirection:isRTL?'row-reverse':'row'}}>
                        <View style={{marginHorizontal:moderateScale(2), flexDirection:isRTL?'row-reverse':'row',flex:1,alignItems:'center',justifyContent:'center'}} >
                            <AppText text={data.price*quantity} color={colors.primaryColor} fontSize={responsiveFontSize(2.5)} fontWeight='800'/> 
                            <AppText text={` ${Strings.sar}`}  color={colors.skipIconColor} fontSize={responsiveFontSize(3)} fontWeight='800'/> 
                        </View>
                       
                            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                                <TouchableOpacity
                                    style={{ alignSelf: isRTL ? 'flex-start' : 'flex-end' }} >
                                    <Icon name='check' color={colors.skipIconColor} size={20} />
                                </TouchableOpacity>
                            </View>
                            
                        
                    </View>
                </View>
            </MyButton>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        marginTop:10,
        height:responsiveHeight(34),
        width: responsiveWidth(45),
        borderRadius: moderateScale(2),
        backgroundColor:'white',
        elevation:2,
        shadowOffset:{width:1,height:2},
        margin: moderateScale(4),
        flexDirection:'column'
    },
    img: {
        width: responsiveWidth(36),
        height: responsiveHeight(17),
        borderRadius:moderateScale(1.5)
    },
    upperView: {
        flex: 3,
        justifyContent:'center',
        alignItems:'center',
    },
    centeralView: {
        flex: 1,
    },
    lowerView: {
        flex: 1,
        flexDirection:'row'
    }
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(NotificationOrderDetailsCard);
