import React,{Component} from 'react';
import {View,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import {Button} from 'native-base';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);



 class OwnerProductCard extends Component {
       
    render(){
        const {data,onPress,isRTL} = this.props;
        return(
            <MyButton 
            onPress={()=>{
                if(onPress){
                   onPress();
                }
            }}
            style={{
                flexDirection:this.props.isRTL?'row-reverse':'row',
                height: responsiveHeight(17),
                width: responsiveWidth(90),
                borderRadius: moderateScale(2),
                borderWidth: 0.4,
                borderColor: '#CCCCCC',
                backgroundColor: 'white',
                elevation: 2,
                shadowOffset: { width: 1, height: 2 },
                alignSelf: 'center',
                marginTop: moderateScale(8),
                marginBottom: moderateScale(2),
            }}
            >

            <View
             style={{
                height:responsiveHeight(17),
                width:responsiveWidth(31),
                borderWidth:1,
                justifyContent:'center',
                alignItems:'center',
                borderColor:'#EEEEEE',          
                borderRightWidth:isRTL?0:0.4,
                borderLeftWidth:isRTL?0.4:0,
            }}>
                    <FastImage resizeMode='contain' style={styles.img} source={{uri:data.img}} />
             </View>

             <View style={{
                 marginHorizontal:moderateScale(2),
                 width:responsiveWidth(55),
                 height:responsiveHeight(17),
                 marginVertical:moderateScale(3.5)
             }}>

             <View style={{height:responsiveHeight(12),justifyContent:'center'}}>
                <AppText text={data.name} color={colors.primaryColor} fontSize={responsiveFontSize(3)} />
                <View style={{ flexDirection:isRTL?'row-reverse':'row'}}>
                    <AppText  text={data.quantity} color={colors.skipIconColor} />
                    <AppText text={` ${Strings.availableInStore}`} color='#CCCCCC' />
                </View>
             </View>

             <View style={{marginHorizontal:moderateScale(3), alignSelf:isRTL?'flex-start':'flex-end'}}>
                <AppText  text={`${data.price} ${Strings.sar}`} color={colors.skipIconColor} fontSize={responsiveFontSize(3)} fontWeight='600' />
             </View>

             </View>

            
                
            </MyButton>
        );
    }
}

const styles = StyleSheet.create({
  
    img: {
        width: responsiveWidth(36),
        height: responsiveHeight(17),
        borderRadius:moderateScale(1.5)
    },
    
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
});

export default connect(mapStateToProps)(OwnerProductCard);
