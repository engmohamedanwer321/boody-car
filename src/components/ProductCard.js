import React,{Component} from 'react';
import {View,StyleSheet,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import AppText from '../common/AppText';
import * as colors from '../assets/colors';
import { connect } from "react-redux";
import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import {Button} from 'native-base';
import withPreventDoubleClick from './withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);


 class ProductCard extends Component {
     state={
         basketColor:null,
         added: false,
     }

    /* componentDidMount(){
        let flag = 0;
        this.props.ordersData.filter(val=>{
           if(val.id == this.props.data.id){
              flag=1;
           }
       })
       if(flag==0){
           this.setState({basketColor:'#DADADA'})
       }
     }*/

     
    render(){
        const {data,remove,removeProduct,buyProduct, onPress,isRTL} = this.props;
        return(
            <MyButton 
            onPress={()=>{
                if(onPress){
                   onPress();
                }
            }}
            style={styles.card}
            >
                <View style={styles.upperView}>
                    <FastImage style={styles.img} source={{uri:data.img}} />
                </View>

                <View style={styles.centeralView}>
                    <View style={{justifyContent:'center',alignItems:'center',width:responsiveWidth(48)}}>
                        <AppText fontWeight='400' color={colors.primaryColor} fontSize={responsiveFontSize(3)} text={data.name} />
                    </View>
                   <View style={{marginTop:moderateScale(2), marginHorizontal:moderateScale(8), flexDirection:isRTL?'row-reverse':'row',alignSelf:isRTL?'flex-end':'flex-start'}}>
                        <AppText   color={colors.skipIconColor} fontSize={responsiveFontSize(2)} text={data.quantity} />
                        <AppText  color='gray' fontSize={responsiveFontSize(2)} text={`  ${Strings.stoke}`} />
                   </View>
                </View>

                <View style={styles.lowerView}>
                    <View style={{alignItems:'center', flex:1,flexDirection:isRTL?'row-reverse':'row'}}>
                        <View style={{marginTop:moderateScale(5), marginHorizontal:moderateScale(8),flexDirection:isRTL?'row-reverse':'row',flex:1,alignItems:'center',justifyContent:'center'}} >
                            <AppText text={data.price} color={colors.primaryColor} fontSize={responsiveFontSize(2.2)} fontWeight='500'/> 
                            <AppText text={` ${Strings.sar}`}  color={colors.skipIconColor} fontSize={responsiveFontSize(2.5)} fontWeight='800'/> 
                        </View>
                        {remove ?
                            <View style={{marginHorizontal:moderateScale(3), alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                                <TouchableOpacity
                                    onPress={()=>{
                                        if(removeProduct){
                                            removeProduct()
                                        }
                                    }}
                                    style={{ alignSelf: isRTL ? 'flex-start' : 'flex-end' }} >
                                    <Icon name='trash-alt' color='#06134C' size={25} />
                                </TouchableOpacity>
                            </View>
                            :
                            <View style={{marginHorizontal:moderateScale(3), alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                                <TouchableOpacity
                                    onPress={() => {
                                        if (buyProduct) {
                                            if(!this.state.added){
                                                buyProduct()
                                                this.setState({ basketColor: colors.skipIconColor,added:true })
                                           }
                                           
                                        }
                                    }}
                                    style={{ alignSelf: isRTL ? 'flex-start' : 'flex-end' }} >
                                    <Icon name='shopping-cart' color={this.state.basketColor ? this.state.basketColor : '#DADADA'} size={25} />
                                </TouchableOpacity>
                            </View>
                        }
                    </View>
                </View>
            </MyButton>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        marginTop:10,
        height:responsiveHeight(30),
        width: responsiveWidth(45),
        borderRadius: moderateScale(2),
        backgroundColor:'white',
        elevation:2,
        shadowOffset:{width:1,height:2},
        marginHorizontal: moderateScale(4),
        flexDirection:'column'
    },
    img: {
        width: responsiveWidth(31),
        height: responsiveHeight(13),
        borderRadius:moderateScale(1.5)
    },
    upperView: {
        flex: 3,
        justifyContent:'center',
        alignItems:'center',
    },
    centeralView: {
        flex: 1,
    },
    lowerView: {
        flex: 1,
        flexDirection:'row',
        marginTop:moderateScale(1.2),
    }
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    ordersData: state.order.ordersData,
});

export default connect(mapStateToProps)(ProductCard);
