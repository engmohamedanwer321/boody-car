import React, { Component } from 'react';
import {
  View
} from 'react-native';
import AppText from '../common/AppText';
import {Icon,Button} from 'native-base';
import { responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import Strings from '../assets/strings';

export default class Skip extends Component {
    render(){
    console.log('s'+this.props.isRTL);
        return(

            <Button transparent >
                <View style={{ flexDirection:this.props.isRTL? 'row-reverse': 'row'}}  >
                    <AppText color={colors.skipIconColor} text={Strings.skip} fontSize={responsiveFontSize(5)} />
                    <Icon name={this.props.isRTL?"chevron-left":"chevron-right"} type="Entypo" color={colors.skipIconColor} style={{color:colors.skipIconColor,backgroundColor:'green',fontSize:10}} size={20} />
                </View>
            </Button>
        );
    }
}