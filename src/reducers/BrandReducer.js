import {GET_TOP_BRANDS,GET_TOP_BRANDS_SUCCESS,GET_TOP_BRANDS_FAILD,LOGOUT} from '../actions/types';

const initState = {
    topBrands: [],
    brandLoading: false,
    errorText:null,
}

const BrandReducer = (state=initState, action) => {
    switch(action.type){
        case GET_TOP_BRANDS:
            return {...state,brandLoading:true};  
        case GET_TOP_BRANDS_SUCCESS:
            return {...state,topBrands:action.payload,brandLoading:false};  
        case GET_TOP_BRANDS_FAILD:
            return {...state,topBrands:false,errorText:action.payload}; 
        case LOGOUT:
            return initState;                     
        default: 
            return state; 
    }
}

export default BrandReducer;