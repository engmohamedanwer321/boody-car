import {
    GET_OWNER_COMPANY_FAILD,GET_OWNER_COMPANY_SUCCESS,GET_OWNER_COMPANY,
    GET_COMPANY,GET_COMPANY_SUCCESS,GET_COMPANY_FAILD,LOGOUT,
    FETCH_COMANY_REFRESH,FETCH_COMANY_REQUEST,FETCH_COMANY_SUCCESS,FETCH_COMANY_FAIL
} from '../actions/types';
import {
    DataProvider,
} from 'recyclerlistview';


const initState = {
    TopCompanies: [],
    companyLoading: false,
    errorText:null,

    loading:false,
    companies: new DataProvider(),
    pages:null,
    refresh: false,

    ownerCompanies:[],
    ownerCompanyLoading:false,
    ownerCompanyErrorText:null,
}

const CompanyReducer = (state=initState, action) => {
    switch(action.type){
        case GET_COMPANY:
            return {...state,companyLoading:true};  
        case GET_COMPANY_SUCCESS:
            return {...state,TopCompanies:action.payload,companyLoading:false};  
        case GET_COMPANY_FAILD:
            return {...state,companyLoading:false,errorText:action.payload}; 
        
        case FETCH_COMANY_REQUEST:
            return { ...state,errorText:null, loading: true, refresh: false };
        case FETCH_COMANY_SUCCESS:
            console.log()
            return {
                ...state,
                companies: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(state.refresh ? [...action.payload] : [...state.companies.getAllData(), ...action.payload]),
                loading: false,
                refresh: false,
                pages: action.pages,
                errorText:null,
            };
        case FETCH_COMANY_FAIL:
            return { ...state, loading: false, refresh: false, errorText: action.payload };
        case FETCH_COMANY_REFRESH:
            return { ...state,errorText:null, refresh: true, loading: false };
        case GET_OWNER_COMPANY :
            return {...state,ownerCompanyLoading:true};
        case GET_OWNER_COMPANY_SUCCESS :
            return {...state,ownerCompanyLoading:false,ownerCompanies:action.payload};
        case GET_OWNER_COMPANY_FAILD :
            return {...state,ownerCompanyLoading:false,ownerCompanyErrorText:action.payload};    
        case LOGOUT:
            return initState; 

      /*  case CLAER_COMPANY_REQUEST:
            return initialState;
        case SELECTED_ORDER_COMPANY:
            return { ...state, selectedItem: action.payload };*/
    
       
            default: 
        return state; 
    }
}

export default CompanyReducer;