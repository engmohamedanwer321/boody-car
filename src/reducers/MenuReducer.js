

import * as types from "../actions/types";


const initialState = {

    item: "MAIN",
    logoutLoading:false,
}

const MenuReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.SELECT_MENU:
            return { item: action.payload };
        case types.LOGOUT_LOADING:
            return { logoutLoading:true };
        case types.LOGOUT_LOADING_END:
            return { logoutLoading:false };    
        case types.LOGOUT:
            return initialState; 
        default:    
            return state;
    }

}



export default MenuReducer;