import {
    GET_OWNER_PRODUCTS,GET_OWNER_PRODUCTS_FAILD,
    GET_TOP_PRODUCT,GET_TOP_PRODUCT_SUCCESS,GET_TOP_PRODUCT_FAILD,LOGOUT,
    FETCH_PRODUCT_REFRESH,FETCH_PRODUCT_REQUEST,FETCH_PRODUCT_SUCCESS,FETCH_PRODUCT_FAIL
} from '../actions/types';
import {
    DataProvider,
} from 'recyclerlistview';

const initState = {
    topProduct: [],
    productLoading: false,
    errorText:null,

    loading:false,
    products: new DataProvider(),
    pages:null,
    refresh: false,

    ownerProducts:[],
}

const ProductReducer = (state=initState, action) => {
    switch(action.type){
        case GET_TOP_PRODUCT:
            return {...state,productLoading:true};  
        case GET_TOP_PRODUCT_SUCCESS:
            return {...state,topProduct:action.payload,productLoading:false};  
        case GET_TOP_PRODUCT_FAILD:
            return {...state,productLoading:false,errorText:action.payload};


        case FETCH_PRODUCT_REQUEST:
            return { ...state,errorText:null, loading: true, refresh: false };
        case FETCH_PRODUCT_SUCCESS:
            console.log()
            return {
                ...state,
                products: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(state.refresh ? [...action.payload] : [...state.products.getAllData(), ...action.payload]),
                loading: false,
                refresh: false,
                pages: action.pages,
                errorText:null,
            };
        case FETCH_PRODUCT_FAIL:
            return { ...state, loading: false, refresh: false, errorText: action.payload };
        case FETCH_PRODUCT_REFRESH:
            return { ...state,errorText:null, refresh: true, loading: false };
        
        case GET_OWNER_PRODUCTS:
            return{ ownerProducts: action.payload}
        case GET_OWNER_PRODUCTS_FAILD:
            return{ errorText: action.payload}        

        case LOGOUT:
            return initState;                       
        default: 
            return state; 
    }
}

export default ProductReducer;