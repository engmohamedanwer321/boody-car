
import * as types from "../actions/types"

const initialState = {
    images: [],
}

const SelectedPhotoReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.SELECTED_IMAGES:

            // console.log('all images =>' + JSON.stringify(action.payload))
            return { ...state, images: [...state.images, ...action.payload] };

        case types.REMOVE_IMAGE:
            console.log('action.payload' + JSON.stringify(action.payload))
            console.log('images' + JSON.stringify(state.images))

            let newImgs = state.images.filter(img => img != action.payload);
            return { ...state, images: newImgs }

        case types.REMOVE_ALL_IMAGE:
            return { ...initialState }
        default:
            return state;
    }

}

export default SelectedPhotoReducer;