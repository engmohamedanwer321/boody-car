import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form';
import MenuReducer from "./MenuReducer";
import NavigationReducer from './NavigationReducer';
import AuthReducer from "./AuthReducer";
import LanguageReducer from './LanguageReducer';
import SelectedPhotoReducer from './SelectedPhotoReducer';
import SignupReducer from './SignupReducer';
import BrandReducer from './BrandReducer';
import ProductReducer from './ProductReducer';
import CompanyReducer from './CompanyReducer';
import NotificationsReducer from './NotificationsReducer';
import OrderReducer from './OrderReducer';
import CategoryReducer from './CategoryReducer';


export default combineReducers({
    form: formReducer,
    menu: MenuReducer,
    navigation: NavigationReducer,
    auth: AuthReducer,
    lang:LanguageReducer,
    allPhotos:SelectedPhotoReducer,
    signup: SignupReducer,
    brand: BrandReducer,
    product: ProductReducer,
    company: CompanyReducer,
    noti: NotificationsReducer,
    order: OrderReducer,
    category: CategoryReducer,
});