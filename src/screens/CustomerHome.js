import React,{Component} from 'react';
import {
     View,TouchableOpacity, Alert,NetInfo,Keyboard,ActivityIndicator,
     Platform,PermissionsAndroid
} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Icon from 'react-native-vector-icons/FontAwesome5';
import strings from '../assets/strings';
import { Button, Item, Picker, Label } from "native-base";
import SnackBar from 'react-native-snackbar-component';
import AppSpinner from '../common/AppSpinner';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import { RNToasty } from 'react-native-toasty';
import { BASE_END_POINT} from '../AppConfig';
import axios from 'axios';
import LoadingOverlay from '../components/LoadingOverlay';
import AppInput from '../common/AppInput';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import LogoutOverlay from '../components/LogoutOverlay';
import {userLocation} from '../actions/OrderAction';
import Permissions from 'react-native-permissions';


const MyButton =  withPreventDoubleClick(Button);
const MyTouchable =  withPreventDoubleClick(TouchableOpacity);



const validate = values => {
    const errors = {};

    const partName = values.partName;
    const phoneNumber = values.phoneNumber;
    

    if (partName == null) {
        errors.partName = strings.require;
    }

    if (phoneNumber == null) {
        errors.phoneNumber = strings.require;
    } else if (phoneNumber.length < 10) {
        errors.phoneNumber = strings.errorPhone;
    }else if(isNaN(Number(phoneNumber))){
        errors.phoneNumber = strings.errorPhoneFormat
    }    
    return errors;
};

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,
            isRTL,iconColor,editable,labelColor,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                labelColor={labelColor}
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class CustomerHome extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    state = {        
        notConnection:null,
        searchRequest:false,   
    };

    permissions = () => {
        Permissions.check('location').then(response => {
            if (response === 'denied' || response === 'undetermined') {
                this._requestPermission()
            } else {
                console.log('permission done')
                this.getLocation();
            }
        });
    }

    _requestPermission = () => {
        Permissions.request('location').then(response => {
            if (response === 'denied' || response === 'undetermined' ) {
                this._requestPermission()
            } else {
                console.log('permission done')
                this.getLocation();
            }
        })
    }

    getLocation = () => {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            // Create the object to update this.state.mapRegion through the onRegionChange function
            let region = {
              latitude:       position.coords.latitude,
              longitude:      position.coords.longitude,
              latitudeDelta:  0.00922*1.5,
              longitudeDelta: 0.00421*1.5
            }

            console.log('region is 222  =>  ')
            console.log(region)
            this.props.userLocation([position.coords.latitude,position.coords.longitude])
           
           // this.onRegionChange(region, region.latitude, region.longitude);
          }, (error)=>console.log(error));
    }


    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false,
        });
    }

    checkPermissionForApiLess23 = async () => {
        try{
            const p = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
            if(p != PermissionsAndroid.RESULTS.GRANTED){
                this.putPermissionForApiLess23()
            }else{
               console.log('pppppppppprrrrr')
               this.getLocation()             
            }
        }catch (err) {
            console.log(err)
          }
    }
    putPermissionForApiLess23 = async () => {
        try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.getLocation()
            } else {
                this.putPermissionForApiLess23()
            }

            
          } catch (err) {
            console.log(err)
          }
    }
    
    componentDidMount(){
        this.enableDrawer();
        if(Platform.Version < 23){
            this.checkPermissionForApiLess23()
        }else{
            this.permissions();
        }
    }



    onSearch(values){
        this.setState({searchRequest:true});
        axios.post(`${BASE_END_POINT}search/guest`, JSON.stringify({ 
            search: values.partName,
            phone: values.phoneNumber,
         }), {
            headers: {
              'Content-Type': 'application/json',
            },
          }).then(response=>{
              this.setState({noConnection:null,searchRequest:false});
            this.props.navigator.push({
                screen:'WaitSearchResponse',
                animated:true,
            })
          }).catch(error=>{
              console.log(error)
            this.setState({searchRequest:false});
              if(!error.response){
                this.setState({noConnection:strings.noConnection});
              }
          });
        
    }
  

    
   leftComponent = () => (
    <View style={{flexDirection:this.props.isRTL?'row-reverse':'row'}}>
        <TouchableOpacity
        onPress={()=>{
            if(this.props.currentUser){
                this.props.navigator.push({
                    screen:'Notifications',
                    animated:true,
                  })
            }else{
                RNToasty.Warn({title:strings.checkUser}) 
            }
        }}
         style={{marginHorizontal:moderateScale(7)}}>
            <Icon name='bell' color='white' size={20} />
        </TouchableOpacity>
        
    </View>
)


    render(){
        return(
            <View style={{flex:1}}>
                <AppHeader centerLogo={true} showCart showBurger leftComponent={this.leftComponent()} navigator={this.props.navigator} />
                <View style={{marginTop:moderateScale(2), justifyContent:'center',alignItems:'center',width:responsiveWidth(100)}}>
                 <View style={{marginTop:moderateScale(10),width:responsiveWidth(90)}}>
                    <Field labelColor='#CCCCCC' borderColor='gray' style={{ width: responsiveHeight(90) }} textColor={colors.darkPrimaryColor} name="partName" isRTL={this.props.isRTL}  marginTop={moderateScale(7)} label={strings.searchByPartNAme} component={InputComponent}
                        returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss()
                        }}
                    />
                 </View>
                 <View style={{width:responsiveWidth(90)}}>
                    <Field labelColor='#CCCCCC' numeric borderColor='gray' style={{ width: responsiveHeight(90) }} textColor={colors.darkPrimaryColor} name="phoneNumber" isRTL={this.props.isRTL}  marginTop={moderateScale(7)} label={strings.phoneNumer} component={InputComponent}
                        returnKeyType="done"
                        onSubmit={() => {
                            Keyboard.dismiss()
                        }}
                    />
                 </View>

                 <View style={{marginTop:moderateScale(22)}}>
                     <MyTouchable
                     onPress={this.props.handleSubmit(this.onSearch.bind(this))}
                      style={{height:responsiveHeight(8), width:responsiveWidth(90), justifyContent:'center',alignItems:'center',backgroundColor:colors.primaryColor,borderRadius:moderateScale(3)}}>
                        
                        {
                            this.state.searchRequest?
                            <ActivityIndicator color='white'/>
                            :
                            <AppText fontSize={responsiveFontSize(3)} text={Strings.search} color='white'/>
                        }                        
                     </MyTouchable>
                 </View>
             </View>
             {this.props.logoutLoading&& <LogoutOverlay title={Strings.waitLogout}/>}
                <SnackBar
                    visible={this.state.noConnection!=null?true:false} 
                    textMessage={this.state.noConnection}
                    messageColor='white'
                    backgroundColor={colors.primaryColor}
                    autoHidingTime={5000}
                    />
                
            </View>
        )
    }
}

const form = reduxForm({
    form: "CustomerHome",
    validate,
})(CustomerHome)


const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser, 
    
  logoutLoading: state.menu.logoutLoading,
    //loading: state.auth.loading,
    

})

const mapDispatchToProps = {
    userLocation,
}

export default connect(mapToStateProps,mapDispatchToProps)(form);