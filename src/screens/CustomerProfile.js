import React,{Component} from 'react';
import {View,TouchableOpacity} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import logout from "../actions/LogoutActions";
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import LogoutOverlay from '../components/LogoutOverlay';

const MyButton =  withPreventDoubleClick(Button);


class CustomerProfile extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    componentDidMount(){
        this.enableDrawer()
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }
    leftComponent = () => (
        <View style={{flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <TouchableOpacity
            onPress={()=>{
                if(this.props.currentUser){
                    this.props.navigator.push({
                        screen:'CustomerUpdateProfile',
                        animated:true,
                      })
                }else{
                    RNToasty.Warn({title:strings.checkUser}) 
                }
            }}
             style={{marginHorizontal:moderateScale(4)}}>
                <Icon type='FontAwesome' name='edit' style={{color:'white'}} size={20} />
            </TouchableOpacity>
            
        </View>
   )
    render(){
        const {navigator,currentUser,userToken} = this.props;
        console.log('m5m5m5m5m5m5m5m5');

        return(
            this.props.currentUser&&
            <View>
                <AppHeader leftComponent={this.leftComponent()} navigator={navigator}  showBurger title={Strings.myProfile} />
                <View style={{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(100), height: responsiveHeight(40), backgroundColor: '#F7F7F7' }}>
                    <Icon name='user' type='FontAwesome' style={{ fontSize: responsiveFontSize(8), color: '#979797' }} />
                    <View style={{width:responsiveWidth(93),justifyContent:'center',alignItems:'center'}}>
                        <AppText textAlign='center' color={colors.primaryColor} fontSize={responsiveFontSize(5)} text={`${currentUser.user.firstname} ${currentUser.user.lastname}`} />
                    </View>
                    <AppText text={currentUser.user.email} color='gray' />
                    <AppText text={currentUser.user.phone} color={colors.primaryColor} />
                    <View style={{marginTop:moderateScale(3)}}>
                        <MyButton
                        onPress={()=>{
                            this.props.navigator.push({
                                 screen: 'ChangePassword',         
                                 animated: true,
                             });
                         }}
                         style={{width:responsiveWidth(60),borderRadius:moderateScale(4), justifyContent:'center',alignItems:'center',backgroundColor:colors.primaryColor}}>
                            <AppText color='white' text={Strings.changPassword} />
                        </MyButton>
                    </View>
                </View>
                <View style={{width:responsiveWidth(100),justifyContent:'center',alignItems:'center',marginTop:moderateScale(20)}}>
                    <View>
                        <MyButton
                        onPress={()=>{
                            this.props.logout(userToken,currentUser.token, navigator);
                        }}
                         transparent style={{borderWidth:1, width:responsiveWidth(60),borderRadius:moderateScale(4), justifyContent:'center',alignItems:'center',borderColor:colors.primaryColor}}>
                            <AppText color={colors.primaryColor} text={Strings.createNewAccount} />
                        </MyButton>
                    </View>
                </View>
                {this.props.logoutLoading&&<LogoutOverlay title={Strings.waitLogout}/>}
            </View>
        );
    }
}

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser : state.auth.currentUser,
    userToken: state.auth.userToken,
    logoutLoading: state.menu.logoutLoading,
});
const mapDispatchToProps = {
    logout,
  }

export default connect(mapStateToProps,mapDispatchToProps)(CustomerProfile);
