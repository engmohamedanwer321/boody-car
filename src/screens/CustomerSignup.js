import React, { Component } from 'react';
import {
  View,StatusBar,TouchableOpacity,Keyboard,ScrollView,Alert
} from 'react-native';
import { connect } from 'react-redux';
import { Button,  Icon, Item, Picker, Label } from "native-base";
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from  '../assets/strings';
import AppSpinner from '../common/AppSpinner';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import Checkbox from 'react-native-custom-checkbox';
import { signup } from '../actions/SignupAction';
import LoadingOverlay from '../components/LoadingOverlay';
import { BASE_END_POINT} from '../AppConfig'
import axios from 'axios';
import SnackBar from 'react-native-snackbar-component';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);



const validate = values => {
    const errors = {};
    var needle = '05';
    if (!values.email) {
        errors.email = Strings.require;
    } else if (
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            values.email,
        )
    ) {
        errors.email = Strings.errorEmail;
    }

    if (!values.firstName) {
        errors.firstName = Strings.require;
    } else if (!isNaN(values.firstName)) {
        errors.firstName = Strings.errorName;
    }

    if (!values.lastName) {
        errors.lastName = Strings.require;
    } else if (!isNaN(values.lastName)) {
        errors.lastName = Strings.errorName;
    }

    if (!values.phoneNumber) {
        errors.phoneNumber = Strings.require;
    } else if (!values.phoneNumber.startsWith('05')) {
        errors.phoneNumber = Strings.errorStartPhone;
    }  else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = Strings.errorPhone;
    }else if(isNaN(Number(values.phoneNumber))){
        errors.phoneNumber = Strings.errorPhoneFormat
    }

    if (!values.password) {
        errors.password = Strings.require;
    }

    if (!values.confirmPassword) {
        errors.confirmPassword = Strings.require;
    } else if (values.password != values.confirmPassword) {
        errors.confirmPassword = Strings.errorConfirmPassword;
    }
    
    if (!values.city) {
        errors.city = Strings.require;
    }

    if (!values.area) {
        errors.area = Strings.require;
    }

    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class CustomerSignup extends Component {

    state = {
        agreeTerms:false,
        citiesLoadig:true,
        cities:[],
        selectedCity:null,
        areaLoading:true,
        areas:[],
        selectedArea:null,
        errorText:null,
        showSnack:false,
    }

     getCities = () => {
           axios.get(`${BASE_END_POINT}cities`)
           .then(response => {
                this.setState({
                   citiesLoadig:false, 
                   cities:response.data,
                   selectedCity:response.data[0].id,
                },()=>this.getAreas(response.data[0].id))
                
           }).catch(error => {
            if (!error.response) {
               this.setState({errorText:Strings.noConnection,showSnack:true}) 
            } 
          });
    }

    getAreas = (cityID) => {
        this.setState({areas:[],areaLoading:true})
           axios.get(`${BASE_END_POINT}cities/${cityID}/areas`)
           .then(response => {
                this.setState({
                    areaLoading:false,
                    areas:response.data,
                    selectedArea:response.data[0].id})
           }).catch(error => {
            console.log(error.response);
            if (!error.response) {
              this.setState({showSnack:true,errorText:Strigs.noConnection,areaLoading:false})
            } 
          });
    }
    

   
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator; 
    }
    
    componentDidMount(){    
        this.disableDrawer();
        this.getCities(); 
        console.log('signup page  '+this.props.userToken);
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    renderScreenTitle = () => (
        <View style={{marginTop:moderateScale(9),alignSelf:this.props.isRTL? 'flex-end':'flex-start',marginHorizontal:moderateScale(10),height:responsiveHeight(20)}}>
            <AppText fontSize={responsiveFontSize(3)} textAlign={this.props.isRTL? 'right' : 'left'} text={Strings.hello} color={colors.skipIconColor}/>
            <AppText fontWeight='500' textAlign={this.props.isRTL? 'right' : 'left'} fontSize={responsiveFontSize(6)} text={Strings.customerAccount} color={colors.primaryColor} />
            
        </View>
    )

    onSignup(values) {
        if(this.state.agreeTerms){
            const user = {
                firstname:values.firstName,
                lastname:values.lastName,
                email:values.email,
                phone:values.phoneNumber,
                password:values.password,
                city:this.state.selectedCity,
                area:this.state.selectedArea,
                type:this.props.clientType,
                token:this.props.userToken
            } 

            this.props.signup(user,this.props.navigator)                 
        }    
    }
    
    renderSignupButtons() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={
                handleSubmit(this.onSignup.bind(this)) 
            }
            style={{opacity:this.state.agreeTerms? 1:0.6, marginTop:moderateScale(10),marginBottom:moderateScale(10), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:colors.primaryColor,borderRadius:moderateScale(2.5)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.signup} color='white' />
            </MyButton>
        );
    }

    renderCityPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(7), marginBottom:moderateScale(5),width:responsiveWidth(80),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
             <Label style={{fontSize:responsiveFontSize(3),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.city}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.citiesLoadig?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedCity}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedCity:value,});
                        this.getAreas(value);
                     }}
                     >
                     {this.state.cities.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.cityName} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderAreaPicker = () => {
        console.log("rtl  "+ this.props.isRTL);
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(7), marginBottom:moderateScale(5),width:responsiveWidth(80),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
               <View style={{ flex:1}}>
               <Label style={{fontSize:responsiveFontSize(3),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.area}</Label>
               </View>
                <View style={{flex:1}}>
                    {
                        this.state.areaLoading?
                        <AppSpinner isRTL={this.props.isRTL} />
                        :
                        <Picker           
                        mode="dropdown"
                        iosHeader="Select your SIM"
                        iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                        selectedValue={this.state.selectedArea}
                        style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                        onValueChange={(value,index)=>{
                           // this.props.setArea(value);
                           this.setState({selectedArea:value});
                        }}
                        >
                       {this.state.areas.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.areaName} value={obj.id} />
                     ))}
                    </Picker>
                    }
                </View>
            </Item>
        )
}
    renderTerms = () => {
        return(
            <View style={{justifyContent:'center',alignItems:'center', marginTop:moderateScale(5), width:responsiveWidth(80), flexDirection:this.props.isRTL?'row-reverse':'row',marginHorizontal:moderateScale(2)}}>
                <View style={{marginRight:3, alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                <Checkbox
                    checked={this.state.agreeTerms}
                    style={{backgroundColor: 'white', color:colors.primaryColor, borderRadius: 5}}
                    onChange={(name, checked) =>{this.setState({agreeTerms:checked})}}
                />
                </View>
                <View style={{ alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                    <AppText text={Strings.term1} color='black' fontSize={this.props.isRTL?responsiveFontSize(2.8):responsiveFontSize(2.5)}/>
                </View>
                <MyTouchableOpacity
                onPress={()=>{
                    this.props.navigator.push({
                        screen: 'TermsAndCondictions',
                        animated: true,
                    })
                }}
                 style={{ borderBottomWidth:1,borderBottomColor:colors.primaryColor, alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                    <AppText text={Strings.term2} fontSize={this.props.isRTL?responsiveFontSize(2.8):responsiveFontSize(2.5)} color={colors.primaryColor}/>
                </MyTouchableOpacity>
                <View style={{alignSelf:this.props.isRTL?'flex-end':'flex-start'}}>
                    <AppText text={Strings.term3} color='black' fontSize={this.props.isRTL?responsiveFontSize(2.8):responsiveFontSize(2.5)}/>
                </View>
            </View>
        )
    }
    
    renderContent() {
       // const { navigator, isRTL } = this.props;
        return (
            <View>

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="firstName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.firstName} component={InputComponent}
                 returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} name="lastName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.lastName} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} email name="email" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.email} component={InputComponent} 
                   returnKeyType="done"
                   onSubmit={() => {
                       Keyboard.dismiss();
                   }}
                />

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} numeric name="phoneNumber" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.phoneNumer} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />

                {this.renderCityPicker()}
                {this.renderAreaPicker()}
                  
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} password  name="password" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.password} component={InputComponent} 
                    returnKeyType="done"
                    onSubmit={() => {
                        Keyboard.dismiss();
                    }}
                />

                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.primaryColor} password  name="confirmPassword" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.confirmPassword} component={InputComponent}
                   returnKeyType="done"
                   onSubmit={() => {
                       Keyboard.dismiss();
                   }}
                />
               
                {this.renderTerms()}
                {this.renderSignupButtons()}
            </View>

        )
    }

    render(){
        return(
            <View style={{ flex:1 }}>
                <StatusBar backgroundColor={colors.darkPrimaryColor} />
                <AppHeader navigator={this.props.navigator} title={Strings.customer} showBack/> 
                <ScrollView style={{ flex:1 }} >   
               {this.renderScreenTitle()}
                <View style={{justifyContent:'center',alignItems:'center', flex:1, marginTop:moderateScale(5),width:responsiveWidth(100)}}>
                    <View style={{width:responsiveWidth(80)}}>
                        {this.renderContent()}
                    </View>
                </View>    
                </ScrollView>
                {this.props.signupLoading && <LoadingOverlay/>}

                {/* for city and area */}
                <SnackBar
                visible={this.state.showSnack} 
                textMessage={this.state.errorText}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />

                 {/* for signup */}
                <SnackBar
                visible={this.props.errorText!=null} 
                textMessage={this.props.errorText}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />

            </View>
            
        );
    }
}

const form = reduxForm({
    form: "CustomerSignup",
    validate,
})(CustomerSignup)

const mapDispatchToProps = {
    signup,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    signupLoading: state.signup.signupLoading,
    clientType: state.signup.clientType,
    errorText: state.signup.errorText,
    userToken: state.auth.userToken,
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

