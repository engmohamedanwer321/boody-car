import React, { Component } from 'react';
import {
  View,TouchableOpacity,Keyboard,AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import {login} from '../actions/AuthActions';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import AppInput from '../common/AppInput';
import AppHeader from '../common/AppHeader';
import Strings from '../assets/strings';
import LoadingOverlay from '../components/LoadingOverlay';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import SnackBar from 'react-native-snackbar-component';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);


const validate = values => {
    const errors = {};

    const phone = values.phone
    const password = values.password;

    if (phone == null) {
        errors.phone = Strings.require;
    }
    if (password == null) {
        errors.password = Strings.require;
    }

    return errors;
};

export let rootNavigator = null

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}

class Login extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props) {
        super(props);
        rootNavigator = this.props.navigator;
    }
    
    componentDidMount(){    
        this.disableDrawer(); 
        console.log('anwer  '+this.props.isRTL) 
        console.log('Login page  '+this.props.userToken)     
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    renderScreenTitle = () => (
        <View style={{marginTop:moderateScale(9),alignSelf:this.props.isRTL? 'flex-end':'flex-start',marginHorizontal:moderateScale(10),height:responsiveHeight(20)}}>
            <AppText fontSize={responsiveFontSize(3)} textAlign={this.props.isRTL? 'right' : 'left'} text={Strings.hello} color={colors.skipIconColor}/>
            <AppText fontWeight='700' textAlign={this.props.isRTL? 'right' : 'left'} fontSize={responsiveFontSize(6)} text={Strings.pageTitle} color={colors.primaryColor} />
         
        </View>
    )

    onLogin(values) {
        AsyncStorage.setItem('@password', values.password);
        this.props.login(
            values.phone,
            values.password,
            this.props.userToken,
            this.props.navigator,
            true
        );
        Keyboard.dismiss();
    }


    renderLoginButtons() {
        const { handleSubmit } = this.props;

        return (
            <MyButton
            onPress={handleSubmit(this.onLogin.bind(this))}
            style={{marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:colors.primaryColor,borderRadius:moderateScale(2.5)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.login} color='white' />
            </MyButton>
        );
    }

    renderForgetPassword() {
        return (
            <MyTouchableOpacity
             onPress={()=>{
                this.props.navigator.push({
                    screen: 'ForgetPassword',
                    animated: true,
                });
             }}
            style={{marginTop:moderateScale(10), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80)}}
            >
                <AppText fontSize={responsiveFontSize(2.5)} text={Strings.forgetPassword} color={colors.primaryColor} />
            </MyTouchableOpacity>
        );
    }
    
    renderContent() {
       // const { navigator, isRTL } = this.props;
        return (
            <View>
                <Field borderColor='gray' style={{ width: responsiveHeight(80) }} textColor={colors.darkPrimaryColor} name="phone" isRTL={this.props.isRTL} numeric marginBottom={moderateScale(3)} label={Strings.phoneNumer} component={InputComponent}
                 returnKeyType="done"
                    onSubmit={() => {
                       Keyboard.dismiss()
                    }}
                />

                <Field borderColor='gray' textColor={colors.darkPrimaryColor} name="password" isRTL={this.props.isRTL} type="password" password={true} label={Strings.password} component={InputComponent}
                    returnKeyType="done"
                    inputRef={el => this.passwordField = el }
                    onSubmit={() => {
                        Keyboard.dismiss()
                    }}
                />

                {this.renderLoginButtons()}
                {this.renderForgetPassword()}
            </View>

        )
    }

    render(){
        return(
            <View style={{ flex:1 }} >
               <AppHeader navigator={this.props.navigator} title={Strings.login} showBack/>
               {this.renderScreenTitle()}
                <View style={{height:responsiveHeight(50),width:responsiveWidth(100),justifyContent:"center",alignItems:'center',marginTop:moderateScale(5)}}>
                    <View style={{width:responsiveWidth(80)}}>
                        {this.renderContent()}
                    </View>
                </View>      
                <MyTouchableOpacity
                onPress={()=>{
                    this.props.navigator.resetTo({
                        screen: 'CustomerHome',
                        animated: true,
                      });
                }}
                 style={{justifyContent:'center',alignItems:'center',alignSelf:this.props.isRTL?'flex-start':'flex-end',flexDirection:this.props.isRTL? 'row-reverse':'row',marginHorizontal:moderateScale(10),marginTop:moderateScale(2) }}>
                    <AppText color={colors.skipIconColor} text={Strings.skip} fontSize={responsiveFontSize(3.5)} />
                    <Icon name={this.props.isRTL?"chevron-left":"chevron-right"} type="Entypo" style={{color:colors.skipIconColor}} size={responsiveFontSize(2)} />
                </MyTouchableOpacity>
                {this.props.loading&&<LoadingOverlay/>}
                <SnackBar
                visible={this.props.errorText!=null?true:false} 
                textMessage={this.props.errorText}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}

const form = reduxForm({
    form: "Login",
    validate,
})(Login)

const mapDispatchToProps = {
    login,
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    loading: state.auth.loading,
    errorText: state.auth.errorText,
    userToken: state.auth.userToken,
   
})


export default connect(mapToStateProps,mapDispatchToProps)(form);

