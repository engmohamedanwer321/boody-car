import React, { Component } from 'react';
import { StyleSheet, Text, View, PermissionsAndroid } from 'react-native';
import { AppHeader, AppFAB } from '../common'
import CameraRollPicker from 'react-native-camera-roll-picker';

import * as actions from '../actions/SelectedMorePhoto';
import { connect } from "react-redux";
import { Button } from 'native-base';
import Permissions from 'react-native-permissions'
// import * as colors
import * as colors from '../assets/colors';
import allStrings from '../assets/strings';

class MorePhotoScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    constructor(props) {
        super(props);

        this.state = {
            num: 0,
            selected: [],
            show: false,
            photoPermission:null
        };
    }

    getSelectedImages(images, current) {
        var num = images.length;

        this.setState({
            num: num,
            selected: images,

        });
    }


    async componentDidMount() {
        Permissions.check('photo').then(response => {
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            console.log('resposnse check => ' + JSON.stringify(response))
            if (response === 'denied' || response === 'undetermined' || response === 'denied') {
                console.log('called if')
                this._requestPermission()
            }
            this.setState({ photoPermission: response })

        })
    }
    _requestPermission() {
        console.log('called request')
        Permissions.request('photo').then(response => {
            if (response === 'denied' || response === 'undetermined' || response === 'denied') {
                this._requestPermission()
            }
            this.setState({ photoPermission: response })
        })
    }
    render() {
        const { navigator } = this.props;
        return (
            <View style={styles.container}>
                <AppHeader showBack title={allStrings.choicePhotosCharge} navigator={navigator} />

                {this.state.photoPermission === 'authorized' &&
                <CameraRollPicker
                    scrollRenderAheadDistance={500}
                    initialListSize={1}
                    pageSize={3}
                    removeClippedSubviews={false}
                    groupTypes='SavedPhotos'
                    batchSize={5}
                    maximum={15}
                    selected={this.state.selected}
                    assetType='Photos'
                    imagesPerRow={3}
                    imageMargin={5}
                    callback={this.getSelectedImages.bind(this)} />
        }
                <AppFAB iconName="md-checkmark" disabled={false} color={colors.colorButtons} onPress={() => {
                    this.props.selectedImages(this.state.selected);
                    this.props.navigator.pop();
                }} />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#F6AE2D',
    },
    content: {
        marginTop: 15,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    text: {
        fontSize: 16,
        alignItems: 'center',
        color: '#fff',
    },
    bold: {
        fontWeight: 'bold',
    },
    info: {
        fontSize: 12,
    },
});

export default connect(null, actions)(MorePhotoScreen);