import React,{Component} from 'react';
import {ActivityIndicator, View,NetInfo,TouchableOpacity,FlatList,ScrollView,Alert} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import { Button, Icon } from 'native-base';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';
import SnackBar from 'react-native-snackbar-component';
import ListFooter from '../components/ListFooter';



class NotificationProductDetails extends Component{

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    state = {
        load:true,
        product:[],
        images:[],
        index:0,
        errorText:null,
    }
    constructor(props){
        super(props);
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getProduct()
            }else{
                this.setState({errorText:Strings.noConnection})
            }
          });
    }

    componentDidMount(){
        this.enableDrawer()
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getProduct()                }
            }
          ); 
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }


    getProduct = () => {
        axios.get(`${BASE_END_POINT}products/${this.props.subject}`)
        .then(response=>{
            this.setState({load:false,product:response.data,images:response.data.img.split(',')});
        }).catch(error=>{
            this.setState({load:false});
            console.log(error);
            console.log(error.response);
        })
    }

    renderSlideShow = () => {
        const {isRTL} = this.props;
        
        return(
            <View style={{marginBottom:moderateScale(6), backgroundColor:'#F7F7F7',width:responsiveWidth(100),height:responsiveHeight(30),justifyContent:'center',alignItems:'center'}}>
                <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                    <TouchableOpacity
                       onPress={()=>{
                        if(this.state.index==0){
                            this.setState({index:0})
                        }else{
                            this.setState({index:this.state.index-1})
                        }
                    }} 
                    >
                        <Icon name={isRTL? 'chevron-right':'chevron-left'} type='Entypo' style={{color:'black'}} />
                    </TouchableOpacity>
                    <FastImage resizeMode='contain' style={{width:responsiveWidth(50),height:responsiveHeight(20), marginHorizontal:moderateScale(15)}} source={{uri:this.state.images[this.state.index]}} />
                    <TouchableOpacity
                    onPress={()=>{
                        if(this.state.index==this.state.images.length-1){
                            this.setState({index:0})
                        }else{
                            this.setState({index:this.state.index+1})
                        }
                    }}
                    >
                        <Icon name={isRTL? 'chevron-left':'chevron-right'} type='Entypo' style={{color:'black'}} />
                    </TouchableOpacity>
                </View>

                <View style={{marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
                   
                    {this.state.images.map((img,i)=>(
                         <TouchableOpacity
                         onPress={()=>{
                             this.setState({index:i})
                         }}
                         style={{borderColor:this.state.index==i? colors.skipIconColor : '#CCCCCC', borderWidth:1, width:responsiveWidth(18),height:responsiveHeight(8), marginHorizontal:moderateScale(2)}}>
                            <FastImage resizeMode='stretch' style={{width:responsiveWidth(18),height:responsiveHeight(8),}} source={{uri:img}} />
                        </TouchableOpacity>
                    ))}
                    
                </View>
            </View>
        )
    }

    renderProductDetails = (type,value) =>{
        const {isRTL} = this.props
        return(
            <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',flexDirection:isRTL?'row-reverse':'row' , width:responsiveWidth(100),height:responsiveHeight(7),justifyContent:'space-between',alignItems:'center'}}>
                <AppText paddingHorizontal={moderateScale(9)} text={type} color='#06134C' fontSize={responsiveFontSize(2.5)} />
                <AppText paddingHorizontal={moderateScale(9)} text={value} color={colors.skipIconColor} fontSize={responsiveFontSize(2)} />
            </View>
        )
    }

    renderContent = () => (
        <View style={{flex:1}}>
            {this.state.load ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ListFooter />
                </View>
                :
                <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: moderateScale(2) }}>
                    {this.renderSlideShow()}
                    {this.renderProductDetails(Strings.productName, this.state.product.name)}
                    {this.renderProductDetails(Strings.price, this.state.product.price)}
                    {this.renderProductDetails(Strings.partCategory, this.state.product.category[0].categoryname)}
                    {this.renderProductDetails(Strings.status, this.state.product.status)}
                    {this.renderProductDetails(Strings.quantity, this.state.product.quantity)}
                    {this.renderProductDetails(Strings.company, this.state.product.company.companyname)}
                    {this.renderProductDetails(Strings.model, this.state.product.model.modelname)}
                    {this.renderProductDetails(Strings.year, this.state.product.year.year)}
                    {this.renderProductDetails(Strings.top, this.state.product.top.toString())}
                </ScrollView>}
        </View>
    )

    render(){
        console.log(this.state.product)
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBack title={Strings.notificationDetails} />

                    {
                        this.state.errorText==null&&
                        this.renderContent()
                            
                    }
                 <SnackBar
                 visible={this.state.errorText!=null?true:false} 
                 textMessage={Strings.noConnection}
                 messageColor='white'
                 backgroundColor={colors.primaryColor}
                 autoHidingTime={5000}
                 />
                              
                    
            </View>
        )
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
})



export default connect(mapToStateProps)(NotificationProductDetails);