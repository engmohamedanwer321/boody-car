import React,{Component} from 'react';
import {NetInfo,ActivityIndicator, View,TouchableOpacity,Image,ScrollView,Keyboard} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import { Button, Icon,Item,Label,Picker } from 'native-base';
import {AddProductToBacket} from '../actions/OrderAction';
import {getOwnerProducts} from '../actions/ProductAction';
import { RNToasty } from 'react-native-toasty';
import AppSpinner from '../common/AppSpinner';
import ImagePicker from 'react-native-image-crop-picker';
import AppInput from '../common/AppInput';
import { Field, reduxForm, change as changeFieldValue } from "redux-form"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import SnackBar from 'react-native-snackbar-component';
import LoadingOverlay from '../components/LoadingOverlay';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);




const validate = values => {
    const errors = {};
    if (!values.productName) {
        errors.productName = Strings.require;
    }
    
    if (!values.productPrice) {
        errors.productPrice = Strings.require;
    }

    if (!values.productQuantity) {
        errors.productQuantity = Strings.require;
    }

    return errors;
};

class InputComponent extends Component {
    render() {
        const {
            inputRef,returnKeyType,onSubmit,onChange,input,label,borderColor,
            type,password, numeric,textColor,icon,iconType,marginBottom,email,
            isRTL,iconColor,editable,isRequired,meta: { touched, error, warning },
        } = this.props;

        let hasError = false;
        if (error !== undefined) {
            hasError = true;
        }
        return (
            <AppInput
                onEndEditing={() => input.onBlur(input.value)}
                onBlur={() => input.onBlur(input.value)}
                onChange={onChange}
                ref={inputRef}
                icon={icon}
                iconType={iconType}
                textColor={textColor}
                marginBottom={marginBottom}
                hasError={hasError && touched}
                error={error}
                input={input}
                label={label}
                type={type}
                isRTL={this.props.isRTL}
                password={password}
                email={email}
                numeric={numeric}
                editable={editable}
                borderColor={borderColor}
                iconColor={iconColor}
                onSubmit={onSubmit}
                blurOnSubmit={false}
                returnKeyType={returnKeyType}
                isRequired={isRequired}
            />
        );
    }
}




class OwnerAddNewProduct extends Component{

    state = {
        
        uploadProduct:false,
        productImages:[],
        noConnection:null,

        selectedPartCategory:this.props.categories[0].id,

        selectedstatus:'new',

        selectedcompany:this.props.ownerCompanies[0].id,

        modelLoading: false,
        models:[],
        selectedmodel:null,

        yearLoading: false,
        years:[],
        selectedyear:null,
    }

    constructor(props){
        super(props);
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getModels();
                this.getYears();
            }else{
                this.setState({noConnection:Strings.noConnection})
            }
          });
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    
    componentDidMount(){
        this.enableDrawer()
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({noConnection:null})
                    this.getModels();
                    this.getYears();
                }
            }
          );   
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    getModels = () => {
        this.setState({modelLoading:true});
        axios.get(`${BASE_END_POINT}models`)
        .then(response=>{
            console.log(response.data.data);
            this.setState({modelLoading:false,selectedmodel:response.data.data[0].id, models:response.data.data}); 
        })
        .catch(error=>{
            this.setState({modelLoading:false});
            if(!error.response){
                this.setState({noConnection:Strings.noConnection});
            }
        })
    }

    getYears = () => {
        this.setState({yearLoading:true});
        axios.get(`${BASE_END_POINT}year`)
        .then(response=>{
            console.log('bog')
            console.log(response.data.data);
            this.setState({yearLoading:false,selectedyear:response.data.data[0].id, years:response.data.data}); 
        })
        .catch(error=>{
            this.setState({yearLoading:false});
            if(!error.response){
                this.setState({noConnection:Strings.noConnection});
            }
        })
    }

    renderImages = () => {
        let count = 0;
        return (
            <View style={{flexDirection:this.props.isRTL?'row-reverse':'row', width:responsiveWidth(100),height:responsiveHeight(25),justifyContent:'center',alignItems:'center'}}>
            {this.state.productImages.map(img=>{
                
                if(count<4){
                    count++;
                    return (
                        <Image
                         resizeMode='contain'
                         source={{uri:img}}
                         style={{borderWidth:1.5,borderColor:'#CCCCCC', marginHorizontal:moderateScale(2),height:responsiveHeight(17),width:responsiveWidth(21),borderRadius:moderateScale(2)}}/>
                    )
                }
            })}           
         </View>
        )
    }

    renderUploadProductImagesPart = () => {
        return(
            <View style={{
                width:responsiveWidth(100),
                height:responsiveHeight(30),
                justifyContent:'center',
                alignItems:'center'
            }}>

                {
                    this.state.productImages.length>0?
                    this.renderImages()
                    :
                    <View>
                        <MyButton
                        onPress={()=>{
                            ImagePicker.openPicker({
                                maxFiles:4,
                                multiple: true,
                                waitAnimationEnd: false,
                                includeExif: true,
                                forceJpg: true,

                              }).then(images => {
                                this.setState({ productImages: images.map(i =>i.path) });
                              });
                        }}
                        transparent
                        style={{height:responsiveHeight(20), marginBottom:moderateScale(3)}}>
                            <Icon   type='Entypo' name='images' style={{fontSize:responsiveFontSize(13),color:colors.skipIconColor}} />
                        </MyButton>
                    </View>
                }

                <AppText text={Strings.uploadImage} color={colors.primaryColor} fontSize={responsiveFontSize(3)} />
                
            </View>
        )
    }
    /*
    his.state.productImages.filter(img=>{
            if(count<4){
                count++;
                return img;
            }
        }))*/

    onAddProdut(values) {
        if(this.state.productImages.length>0){
            this.setState({uploadProduct:true});
            var data = new FormData();
            let count = 0;
            data.append('name',values.productName);
            data.append('price',values.productPrice)
            data.append('quantity',values.productQuantity)
            data.append('status',this.state.selectedstatus)
            data.append('year',this.state.selectedyear)
            this.state.productImages.filter(img=>{
                if(count<4){
                    count++;
                    data.append('img',{
                        uri: img,
                        type: 'multipart/form-data',
                        name: 'productImages'
                    }) 
                }
            })
              
            data.append('company',this.state.selectedcompany)
            data.append('model',this.state.selectedmodel)
            data.append('category',this.state.selectedPartCategory)
            
            console.log(data);
            
            axios.post(`${BASE_END_POINT}products`, data, {
                 headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                     //this.props.currentUser.token
                    'Authorization': `Bearer ${this.props.currentUser.token}`    
                 } 
            })
            .then(response=>{
                console.log(response);
                console.log('okosoksoksoksoskoskos')
                this.setState({uploadProduct:false})
                if(this.props.refresh){
                    this.props.getOwnerProducts(this.props.currentUser.user.id)
                }
                    this.props.navigator.push({
                        screen: 'Success',
                        animated: true,
                    })
                
                
            }).catch(error=>{
                console.log(error);
                console.log(error.response);
                this.setState({uploadProduct:false})
            })      
        }else{
            RNToasty.Warn({title:Strings.pleaseSelectProductImages})
        }
    }
    renderAddButton() {
        const { handleSubmit } = this.props;
        return (
            <MyButton 
            onPress={handleSubmit(this.onAddProdut.bind(this))}
            style={{alignSelf:'center', marginTop:moderateScale(10),marginBottom:moderateScale(10), justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(88),backgroundColor:colors.primaryColor,borderRadius:moderateScale(2.5)}}
            >
                <AppText fontSize={responsiveFontSize(3)} text={Strings.addProduct} color='white' />
            </MyButton>
        );
    }

    renderPartCategoryPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.4),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.partCategory}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.props.categoryLoad?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedPartCategory}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedPartCategory:value});
                     }}
                     >
                     {this.props.categories.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.categoryname} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderStatusPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.4),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.status}</Label>
            </View>
             <View style={{flex:1}}>
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedstatus}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedstatus:value});
                     }}
                     >
                     <Picker.Item key='new' label={Strings.new} value='new' />
                     <Picker.Item key='old' label={Strings.old} value='old' />
                     <Picker.Item key='used' label={Strings.used} value='used' />
                     <Picker.Item key='newSemi' label={Strings.newSemi} value='newSemi' />
                 
                 </Picker>
             </View>
         </Item>
        )
    }

    renderCompanyPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.4),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.company}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.props.ownerCompanyLoading?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedcompany}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        
                     }}
                     >
                     {this.props.ownerCompanies.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.companyname} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderModelPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.4),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.model}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.modelLoading?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedmodel}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedmodel:value});
                     }}
                     >
                     {this.state.models.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.modelname} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }

    renderYearPicker = () => {
        return(
            <Item style={{borderBottomColor:'gray', marginTop:moderateScale(10), marginBottom:moderateScale(5),width:responsiveWidth(90),borderWidth:2,flexDirection:this.props.isRTL?'row-reverse':'row'}}>
            <View style={{ flex:1}}>
                <Label style={{fontSize:responsiveFontSize(2.4),color:'#73231F20',alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start'}}>{Strings.year}</Label>
            </View>
             <View style={{flex:1}}>
                 {
                     this.state.yearLoading?
                     <AppSpinner isRTL={this.props.isRTL}/>
                     :
                     <Picker           
                     mode="dropdown"
                     iosHeader="Select your SIM"
                     iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}  
                     selectedValue={this.state.selectedyear}
                     style={{alignSelf: this.props.isRTL? 'flex-start':'flex-end' , height:responsiveHeight(2), borderWidth:2, width:responsiveWidth(35),}}
                     onValueChange={(value,index)=>{
                        this.setState({selectedyear:value});
                     }}
                     >
                     {this.state.years.map(obj=>(
                        <Picker.Item key={obj.id} label={obj.year.toString()} value={obj.id} />
                     ))}
                    
                 </Picker>
                 }
             </View>
         </Item>
        )
    }
    renderFileds() {
        // const { navigator, isRTL } = this.props;
         return (
             <View style={{marginTop:moderateScale(8)}}>
 
                 <Field borderColor='gray' style={{ width: responsiveWidth(90) }} textColor={colors.primaryColor} name="productName" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.productName} component={InputComponent} returnKeyType="ok"
                     onSubmit={() => {
                         Keyboard.dismiss()
                     }}
                 />
                 <Field numeric borderColor='gray' style={{ width: responsiveWidth(90) }} textColor={colors.primaryColor} name="productPrice" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.price} component={InputComponent} returnKeyType="ok"
                     onSubmit={() => {
                        Keyboard.dismiss()
                     }}
                 />
                 <Field numeric borderColor='gray' style={{ width: responsiveWidth(90) }} textColor={colors.primaryColor} email name="productQuantity" isRTL={this.props.isRTL}  marginBottom={moderateScale(3)} label={Strings.quantity} component={InputComponent} returnKeyType="ok"
                     onSubmit={() => {
                        Keyboard.dismiss()
                     }}
                 />

                 {this.renderPartCategoryPicker()}
                 {this.renderStatusPicker()}
                 {this.renderCompanyPicker()}
                 {this.renderModelPicker()}
                 {this.renderYearPicker()}
                
             </View>
 
         )
     }

     renderNoConnection = () => (
        <SnackBar
        visible={this.state.noConnection!=null?true:false} 
        textMessage={Strings.noConnection}
        messageColor='white'
        backgroundColor={colors.primaryColor}
        autoHidingTime={5000}
        />
     )

    render(){
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBack title={Strings.addProduct} />
                 <ScrollView>
                    {this.renderUploadProductImagesPart()}
                    <View style={{alignSelf:'center', width:responsiveWidth(90)}}>
                        {this.renderFileds()}
                    </View>
                    {this.renderAddButton()}
                </ScrollView> 
                {this.renderNoConnection()}
                {this.state.uploadProduct&&<LoadingOverlay/>}        
            </View>          
        )
    }
}

const form = reduxForm({
    form: "OwnerAddNewProduct",
    validate,
})(OwnerAddNewProduct)


const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    categoryLoad: state.category.categoryLoad,
    categories: state.category.categories,
    errorText: state.category.errorText,
    ownerCompanies: state.company.ownerCompanies,
    ownerCompanyLoading: state.company.ownerCompanyLoading,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    AddProductToBacket,
    getOwnerProducts,
}

export default connect(mapToStateProps,mapDispatchToProps)(form);