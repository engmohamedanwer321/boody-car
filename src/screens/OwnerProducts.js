import React,{Component} from 'react';
import {RefreshControl, View,TouchableOpacity,StyleSheet,NetInfo} from 'react-native';
import {  moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Button,Thumbnail} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import SnackBar from 'react-native-snackbar-component';
import ActionButton from 'react-native-action-button';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import AppSpinner from '../common/AppSpinner';
import OwnerProductCard from '../components/OwnerProductCard';
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import ListFooter from '../components/ListFooter';




class OwnerProducts extends Component {
    page=1;
    state = {
        errorText: null,
        productLoading: false,
        products: new DataProvider(),
        pages: null,
        refresh: false,
    }

    constructor(props) {
        super(props);    
        this.renderLayoutProvider = new LayoutProvider(
          () => 1,
          (type, dim) => {
            dim.width = responsiveWidth(100);
            dim.height = responsiveHeight(20);
          },
        );
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.getProductsUnderCategory(1,false);
            }else{
                this.setState({errorText:Strings.noConnection})
            }
          });
      }


    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    componentDidMount(){    
        this.disableDrawer();
        NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.getProductsUnderCategory(1,false);
                }
            }
          );        
    }

    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }
 

   renderRow = (type, data, row) => {
    return (
   <View style={{justifyContent:'center',alignItems:'center'}}>
       <OwnerProductCard 
       onPress={()=>{
           this.props.navigator.push({
               screen:'OwnerProductDetails',
               animated:true,
               passProps: {productData:data}
           })
       }}
       data={data}
       />
   </View> 
    );
   }

   renderFooter = () => {
    return (
      this.state.productLoading ?
        <View style={{alignSelf:'center', margin: moderateScale(5) }}>
         <ListFooter/>
        </View>
        : null
    )
  }

  getProductsUnderCategory = (page,refresh) => {
   
        let uri=`${BASE_END_POINT}products/categories/${this.props.categoryID}/products?page=${page}&limit=10`
        if(refresh){
           this.setState({refresh:true,productLoading:false})
        }else{
            this.setState({refresh:false,productLoading:true})
        }
        axios.get(uri)
        .then(response=>{  
            if(page==1){
                if(response.data.data.length==0){
                    RNToasty.Info({title:Strings.notNotificatios})
                }
            }      
            this.setState({
                productLoading:false,
                refresh:false,
                pages:response.data.pageCount,
                errorText:null,
                products: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.products.getAllData(), ...response.data.data]),
            })
        }).catch(error=>{
            console.log(error.response);
            if (!error.response) {
                this.setState({
                    errorText:Strigs.noConnection,
                    productLoading:false,
                    refresh:false,
                })
              }
        })
    } 
   
    render(){
        return(
            <View style={{flex:1}}>
               <AppHeader  title={Strings.myProducts} showBack navigator={this.props.navigator}/>

                <RecyclerListView
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={this.state.products}
                    rowRenderer={this.renderRow}
                    renderFooter={this.renderFooter}
                    onEndReached={() => {
                        if (this.page < this.state.pages) {
                            this.page++;
                            this.getProductsUnderCategory(this.page, false);
                        }

                    }}
                    refreshControl={<RefreshControl colors={["#B7ED03"]}
                        refreshing={this.state.refresh}
                        onRefresh={() => {
                            this.page = 1
                            this.getProductsUnderCategory(this.page, true);
                        }
                        } />}
                    onEndReachedThreshold={.5}

                />
                <ActionButton
                    onPress={() => {
                        this.props.navigator.push({
                            screen:'OwnerAddNewProduct',
                            animated:true,
                        })
                    }}
                    active={false}
                    hideShadow={true}
                    bgColor='rgba(0,0,0,0.6)'
                    position={this.props.isRTL ? 'left' : 'right'}
                    buttonColor={colors.skipIconColor}
                    renderIcon={() => <Icon color='white' size={20} name='plus' />}
                />

                <SnackBar
                visible={this.state.errorText!=null?true:false} 
                textMessage={Strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View> 

        )
    }
}

const styles = StyleSheet.create({
    footerSpiner: {
        alignItems: 'center',
        alignSelf: "flex-end",
        flex: 1,
        width: '100%',
        marginVertical: 10,
        backgroundColor: colors.primaryColor
      }
    
})

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
    orders: state.order.orders,
})

export default connect(mapToStateProps)(OwnerProducts);
