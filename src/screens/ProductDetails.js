import React,{Component} from 'react';
import {ActivityIndicator, View,ImageBackground,TouchableOpacity,FlatList,ScrollView,Alert} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import FastImage from 'react-native-fast-image'
import Strings from '../assets/strings';
import { Button, Icon } from 'native-base';
import {getTopBrand} from '../actions/BrandAction';
import {getTopProducts} from '../actions/ProductAction';
import {getTopCompanies} from '../actions/CompanyAction';
import {AddProductToBacket} from '../actions/OrderAction';
import { RNToasty } from 'react-native-toasty';


class ProductDetails extends Component{

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    state = {
        showCounter:false,
        count:1,
        index:0,
        images: this.props.productData.img.split(','),
    }

    componentDidMount(){
        this.enableDrawer()
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    renderSlideShow = () => {
        const {isRTL} = this.props;
        
        return(
            <View style={{marginBottom:moderateScale(6), backgroundColor:'#F7F7F7',width:responsiveWidth(100),height:responsiveHeight(30),justifyContent:'center',alignItems:'center'}}>
                <View style={{justifyContent:'center',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                    <TouchableOpacity
                       onPress={()=>{
                        if(this.state.index==0){
                            this.setState({index:0})
                        }else{
                            this.setState({index:this.state.index-1})
                        }
                    }} 
                    >
                        <Icon name={isRTL? 'chevron-right':'chevron-left'} type='Entypo' style={{color:'black'}} />
                    </TouchableOpacity>
                    <FastImage resizeMode='contain' style={{width:responsiveWidth(50),height:responsiveHeight(20), marginHorizontal:moderateScale(15)}} source={{uri:this.state.images[this.state.index]}} />
                    <TouchableOpacity
                    onPress={()=>{
                        if(this.state.index==this.state.images.length-1){
                            this.setState({index:0})
                        }else{
                            this.setState({index:this.state.index+1})
                        }
                    }}
                    >
                        <Icon name={isRTL? 'chevron-left':'chevron-right'} type='Entypo' style={{color:'black'}} />
                    </TouchableOpacity>
                </View>

                <View style={{marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
                   
                    {this.state.images.map((img,i)=>(
                         <TouchableOpacity
                         onPress={()=>{
                             this.setState({index:i})
                         }}
                         style={{borderColor:this.state.index==i? colors.skipIconColor : '#CCCCCC', borderWidth:1, width:responsiveWidth(18),height:responsiveHeight(8), marginHorizontal:moderateScale(2)}}>
                            <FastImage resizeMode='stretch' style={{width:responsiveWidth(18),height:responsiveHeight(8),}} source={{uri:img}} />
                        </TouchableOpacity>
                    ))}
                    
                </View>
            </View>
        )
    }

    renderProductDetails = (type,value) =>{
        const {isRTL} = this.props
        return(
            <View style={{borderTopWidth:1,borderTopColor:'#DEDEDE',flexDirection:isRTL?'row-reverse':'row' , width:responsiveWidth(100),height:responsiveHeight(7),justifyContent:'space-between',alignItems:'center'}}>
                <AppText paddingHorizontal={moderateScale(9)} text={type} color='#06134C' fontSize={responsiveFontSize(2.5)} />
                <AppText paddingHorizontal={moderateScale(9)} text={value} color={colors.skipIconColor} fontSize={responsiveFontSize(2)} />
            </View>
        )
    }

    render(){
        const {productData,AddProductToBacket} = this.props;
        console.log(productData.category)
        console.log(productData.company)
        console.log(productData.model)
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBack title={this.props.productData.name} />
                 <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:!this.props.currentUser?moderateScale(0):!this.state.showCounter?moderateScale(20):moderateScale(7)}}>
                     {this.renderSlideShow()} 
                     {this.renderProductDetails(Strings.productName,productData.name)}
                     {this.renderProductDetails(Strings.price,productData.price)}
                     {this.renderProductDetails(Strings.partCategory,productData.category.categoryname)} 
                     {this.renderProductDetails(Strings.status,productData.status)}
                     {this.renderProductDetails(Strings.quantity,productData.quantity)}
                     {this.renderProductDetails(Strings.company,productData.company.companyname)} 
                    {this.renderProductDetails(Strings.model,productData.model.modelname)}
                    {this.renderProductDetails(Strings.year,productData.yearofmanufacture)}
                     
                                      
                 </ScrollView>        
                 {
                 this.state.showCounter&&
                 <View style={{marginBottom:moderateScale(20),justifyContent:'center',alignItems:'center',flexDirection:this.props.isRTL?'row-reverse':'row', height:responsiveHeight(15),backgroundColor:'#F7F7F7'}}>
                    <View>
                        <TouchableOpacity 
                        onPress={()=>{
                            if(this.state.count!=1){
                                this.setState({count:this.state.count-1})
                            }             
                        }}
                        style={{borderRadius:moderateScale(2), backgroundColor:'#F2414E',justifyContent:'center',alignItems:'center',width:responsiveWidth(11),height:responsiveHeight(7)}}>
                            <Icon name='minus' type='MaterialCommunityIcons' style={{color:'white'}} />
                        </TouchableOpacity>
                    </View >
                    <View style={{marginHorizontal:moderateScale(10)}}>
                        <AppText text={this.state.count}color='#06134C' fontSize={responsiveFontSize(4)} />
                    </View>
                    <View>
                        <TouchableOpacity
                        onPress={()=>{
                            this.setState({count:this.state.count+1})          
                        }}
                         style={{borderRadius:moderateScale(2),backgroundColor:'#82C141',justifyContent:'center',alignItems:'center',width:responsiveWidth(11),height:responsiveHeight(7)}}>
                            <Icon name='plus' type='MaterialCommunityIcons' style={{color:'white'}} />
                        </TouchableOpacity>
                    </View>
                 </View>
                 }

                {
                    this.props.currentUser&&
                    <Button
                    onPress={() => { 
                        if(this.state.showCounter){
                          AddProductToBacket({
                                product:productData.id,
                                count:this.state.count
                            },productData)
                            RNToasty.Success({title:Strings.addProductSuccessfuly})
                        }else{
                            this.setState({showCounter:true})
                        }                
                    }}
                    style={{ width: responsiveWidth(100), justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 0, backgroundColor: '#82C141' }}>
                    <AppText fontSize={responsiveFontSize(3)} color='white' text={this.state.showCounter? Strings.addProductToBacket : Strings.buyProduct} />
                </Button>
                }
            </View>
        )
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    AddProductToBacket,
}

export default connect(mapToStateProps,mapDispatchToProps)(ProductDetails);