import React,{Component} from 'react';
import {View,RefreshControl,StyleSheet,NetInfo} from 'react-native';
import { moderateScale, responsiveWidth,  responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import {Icon,Button} from 'native-base';
import * as colors from '../assets/colors'
import AppText from '../common/AppText';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import {
    RecyclerListView,
    DataProvider,
    LayoutProvider,
} from 'recyclerlistview';
import AppSpinner from '../common/AppSpinner';
import SnackBar from 'react-native-snackbar-component';
import ProductCard from '../components/ProductCard';
import LoadingOverlay from '../components/LoadingOverlay';
import {AddProductToBacket} from '../actions/OrderAction';
import withPreventDoubleClick from '../components/withPreventDoubleClick';
import ListFooter from '../components/ListFooter';


const MyButton =  withPreventDoubleClick(Button);

class SearchResults extends Component {

    page=1;

    state = {
        loading: false,
        errorText: null,
        productLoading: false,
        products: new DataProvider(),
        pages: null,
        refresh: false,
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    }

    constructor(props) {
        super(props);    
        NetInfo.isConnected.fetch().then(isConnected => {
            if(isConnected){
                this.searchProducts(this.page,false);
            }else{
                this.setState({errorText:Strings.noConnection})
            }
          });
        this.renderLayoutProvider = new LayoutProvider(
          () => 2,
          (type, dim) => {
            dim.width = responsiveWidth(50);
            dim.height = responsiveHeight(39);
          },
        );
      }

    componentDidMount(){
        this.enableDrawer()      
          NetInfo.isConnected.addEventListener(
            'connectionChange',
             (isConnected)=>{
                if(isConnected){
                    this.setState({errorText:null})
                    this.searchProducts(this.page,false);
                }
            }
          );
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled:false
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false
        });
    }

    normalSearch = (page,refresh) => {
        let uri=`${BASE_END_POINT}search/normal?page=${page}&limit=10`
        if(refresh){
           this.setState({refresh:true,productLoading:false})
        }else{
            this.setState({refresh:false,productLoading:true})
        }
        axios.post(uri,JSON.stringify(this.props.searchData),{
            headers: {
              'Content-Type': 'application/json',
            },
          })
        .then(response=>{        
            console.log('8888888888')
            console.log(response.data);
            this.setState({
                productLoading:false,
                refresh:false,
                pages:response.data.pageCount,
                errorText:null,
                products: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.products.getAllData(), ...response.data.data]),
            })
            if(response.data.data.length==0){
                RNToasty.Info({title:Strings.notResults})
            }
        }).catch(error=>{
            console.log(error.response);
            if (!error.response) {
                this.setState({
                    errorText:Strigs.noConnection,
                    productLoading:false,
                    refresh:false,
                })
              }
        })
    }
    advancedSearch = (page,refresh) => {
        let uri=`${BASE_END_POINT}search/products?${this.props.params}&page=${page}&limit=10`
        if(refresh){
           this.setState({refresh:true,productLoading:false})
        }else{
            this.setState({refresh:false,productLoading:true})
        }
        axios.get(uri)
        .then(response=>{        
            console.log('99999998888888888')
            console.log(response.data);
            this.setState({
                productLoading:false,
                refresh:false,
                pages:response.data.pageCount,
                errorText:null,
                products: new DataProvider((r1, r2) => r1.id !== r2.id).cloneWithRows(this.state.refresh ? [...response.data.data] : [...this.state.products.getAllData(), ...response.data.data]),
            })
            if(response.data.data.length==0){
                RNToasty.Info({title:Strings.notResults})
            }
        }).catch(error=>{
            console.log(error.response);
            if (!error.response) {
                this.setState({
                    errorText:Strigs.noConnection,
                    productLoading:false,
                    refresh:false,
                })
              }
        })
    }
    searchProducts = (page,refresh) => {
            if(this.props.source=='normal'){
               this.normalSearch(page,refresh);
            }else{
                this.advancedSearch(page,refresh);
            }         
    }
    

    renderRow = (type, data, row) => {
     return (
    <View style={{marginTop:moderateScale(3), justifyContent:'center',alignItems:'center'}}>
        <ProductCard 
        onPress={()=>{
            this.props.navigator.push({
                screen:'ProductDetails',
                animated:true,
                passProps: {productData:data}
            })
        }}
        buyProduct={()=>{
            this.props.AddProductToBacket({
                product:data.id,
                count:1
            },data)
            RNToasty.Info({title:Strings.addProductSuccessfuly})
        }}
        data={data}
        />
    </View> 
     );
    }

    renderFooter = () => {
        return (
          this.state.productLoading ?
            <View style={{alignSelf:'center', margin: moderateScale(5) }}>
              <ListFooter/>
            </View>
            : null
        )
      }

   sendToAdmin = (partName) => {
       this.setState({loading:true});
    axios.post(`${BASE_END_POINT}search`, JSON.stringify(this.props.searchData), {
        headers: {
          'Content-Type': 'application/json',
          //this.props.currentUser.token
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      }).then(response=>{
        this.setState({loading:false});
          RNToasty.Success({title:Strings.sendSuccessful})
      }).catch(error=>{
        this.setState({loading:false});
        RNToasty.Error({title:Strings.sendSuccessful})
      })
   }

    render(){
        const {navigator,currentUser,partName} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader title={Strings.searchResults} showBack navigator={navigator}/>
                {
                    this.state.errorText==null&&
                    <View style={{flex:1}} >
                    <RecyclerListView            
                    layoutProvider={this.renderLayoutProvider}
                    dataProvider={this.state.products}
                    rowRenderer={this.renderRow}
                    renderFooter={this.renderFooter}
                    onEndReached={() => {
                       
                        if(this.page < this.state.pages){
                            this.page++;
                            this.searchProducts(this.page, false);
                        }
                        
                      }}
                      refreshControl={<RefreshControl colors={["#B7ED03"]}
                        refreshing={this.state.refresh}
                        onRefresh={() => {
                          this.page = 1
                          this.searchProducts(this.page, true);
                        }
                        } />}
                      onEndReachedThreshold={.5}
            
                />
                <MyButton
                onPress={()=>{
                    this.sendToAdmin(this.props.partName);
                }}
                 style={{marginTop:moderateScale(6), width:responsiveWidth(100), justifyContent:'center',alignItems:'center',position: 'absolute',bottom:0, backgroundColor:'#82C141'}}>
                    <AppText fontSize={responsiveFontSize(3)} color='white' text={Strings.sendToAdmin} />
                </MyButton>
                </View>
                }
                {this.state.loading&&<LoadingOverlay/>}
                 <SnackBar
                visible={this.state.errorText!=null?true:false} 
                textMessage={Strings.noConnection}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    footerSpiner: {
        alignItems: 'center',
        alignSelf: "flex-end",
        flex: 1,
        width: '100%',
        marginVertical: 10,
        backgroundColor: colors.primaryColor
      }
    
})

const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser, 
    /*products: state.product.products,
    loading: state.product.loading,
    refresh: state.product.refresh,
    pages: state.product.pages,
    errorText: state.product.errorText,*/
})

const mapDispatchToProps = {
    AddProductToBacket,
    //searchProducts,
}

export default connect(mapStateToProps,mapDispatchToProps)(SearchResults);
