import React,{Component} from 'react';
import {View,Alert,AsyncStorage} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import {Picker,Item,Icon,Label} from 'native-base';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import Strings from '../assets/strings';
import AppText from '../common/AppText'
import  changeLanguage from '../actions/LanguageActions';



class SelectLanguage extends Component {

    state = {
        lang:this.props.isRTL?1:2,
       
    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

 
    renderLanguagePicker = () => {
        return(
            <Item style={{ alignSelf:'center', borderBottomColor: 'gray', marginTop: moderateScale(20), marginBottom: moderateScale(5), width: responsiveWidth(80), borderWidth: 2, flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
                <View style={{ flex: 1 }}>
                    <Label style={{ fontSize: responsiveFontSize(2), color: 'gray', alignSelf: this.props.isRTL ? 'flex-end' : 'flex-start' }}>{Strings.selcetLanguage}</Label>
                </View>
                <View style={{ flex: 1 }}>
                    <Picker
                        mode="dropdown"
                        iosHeader="Select your SIM"
                        iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "black", fontSize: 25 }} />}
                        selectedValue={this.state.lang}
                        style={{ alignSelf: this.props.isRTL ? 'flex-start' : 'flex-end', height: responsiveHeight(2), borderWidth: 2, width: responsiveWidth(35), }}
                        onValueChange={async (value, index) => {
                           if(value==1){
                            await AsyncStorage.setItem('@lang','ar');
                            Strings.setLanguage('ar')
                            this.props.changeLanguage(true)
                            
                           }else{
                            await AsyncStorage.setItem('@lang','en');
                            Strings.setLanguage('en')
                            this.props.changeLanguage(false)
                            
                           }
                           this.setState({lang:value})
                        }}
                    >
                       <Picker.Item label={Strings.arabic} value={1} />
                       <Picker.Item label={Strings.english} value={2} />
                    </Picker>

                </View>
            </Item>
        )
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: this.props.isRTL?false:true
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: this.props.isRTL?true:false
        });
    }
    
    selectLang = async (id) => {
        console.log(id)
       
        if(id==1){
            await AsyncStorage.setItem('@lang','ar');
            Strings.setLanguage('ar')
            this.props.changeLanguage(true)
            this.setState({lang:1})
            
        }else{
            await AsyncStorage.setItem('@lang','en');
            Strings.setLanguage('en')
            this.props.changeLanguage(false)
            this.setState({lang:2})
           
        }  
        
        
    }

    render(){
        this.enableDrawer()
         const {navigator,isRTL} = this.props;
        return(
            <View style={{flex:1}}>
                <AppHeader navigator={navigator} title={Strings.selcetLanguage} showBurger={this.props.currentUser.user.type=='CLIENT'&&true} showBack={this.props.currentUser.user.type=='OWNER'&&true} />
                
                {this.renderLanguagePicker()}
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    changeLanguage,
}


export default connect(mapToStateProps,mapDispatchToProps)(SelectLanguage);