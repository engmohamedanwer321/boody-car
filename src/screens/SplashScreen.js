import React, { Component } from 'react';
import {
  View,Image,ImageBackground,AsyncStorage,StatusBar,Alert,AppState,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { responsiveHeight, responsiveWidth, moderateScale,responsiveFontSize } from "../utils/responsiveDimensions";
import * as Progress from 'react-native-progress';
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import {CheckLoginWhenOpen,userToken,login} from '../actions/AuthActions';
import {userLocation} from '../actions/OrderAction';
import  changeLanguage from '../actions/LanguageActions';
import SnackBar from 'react-native-snackbar-component';
import Permissions from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box"
import FCM, { NotificationActionType, FCMEvent } from "react-native-fcm";
import AkfaNotificationCard from '../components/AkfaNotificationCard';
import { BASE_END_POINT } from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import axios from 'axios';


// 01201320304
//12345678   
class SplashScreen extends Component {
    pass;
    state={
        showSnack: false,
        latLocation:null,
        lngLocation:null,
        notiFlag:0,
        stopNotiClick:false,
       
    } 

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    constructor(props){
        super(props); 
        FCM.on(FCMEvent.Notification, async (notif) => {
            console.log('Recived');
            console.log(notif.fcm.icon); 
           
            if(!notif.local_notification){
                this.setState({notiFlag:1})
              if(AppState.currentState === 'active' ) {
                this.ServerNotification(notif.fcm.title,notif.fcm.body,true);
              }else if(AppState.currentState === 'background') {
                this.ServerNotification(notif.fcm.title,notif.fcm.body,false);
              }
            }

            if(this.state.notiFlag != 0 && this.state.stopNotiClick==false){
                console.log('kow omk')
                console.log(notif.fcm.body)
                if(notif.fcm.body == 'accept your rigister' ){
                    this.props.login(this.props.currentUser.user.phone,this.pass,this.props.userTokens,this.props.navigator,false)
                    this.setState({stopNotiClick:true})                  
                } 
            }
        });              
        this.checkLanguage();
    }

    

    checkToken = async (token) => {
        t = await AsyncStorage.getItem('@BoodCarToken')
        if(t){
            this.props.userToken(t);
        }else{
            AsyncStorage.setItem('@BoodCarToken',token)
            this.props.userToken(token)
        }

        console.log("My Token")
        console.log(t)
        console.log(token)
    }
    
    componentDidMount(){  
        FCM.getFCMToken().then(token => {
            console.log(`TOKEN (getFCMToken)  ${token}`);
            this.checkToken(token);
            });
        this.disableDrawer();
       
       this.checkLogin();
       FCM.on(FCMEvent.RefreshToken, (token) => {

        axios.put(`${BASE_END_POINT}updateToken`, JSON.stringify({
            oldToken: this.props.userTokens,
            newToken: token,
          }), {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        }).then(rsponse=>{
            console.log("done done")
            AsyncStorage.setItem('@BoodCarToken',token)
            this.props.userToken(token)
        }).catch(error=>{
            console.log("token Error")
            console.log(error)
            console.log(error.response)
        })

        });

        
     
    }

        
    ServerNotification = (title,body,val) => {
        FCM.presentLocalNotification({
        channel: 'default',
        id: new Date().valueOf().toString(), // (optional for instant notification)
        title: title, // as FCM payload
        body: body, // as FCM payload (required)
        sound: "bell.mp3", // "default" or filename
        priority: "high", // as FCM payload
        click_action: "com.myapp.MyCategory", // as FCM payload - this is used as category identifier on iOS.
       
        ticker: "My Notification Ticker", // Android only
        auto_cancel: true, // Android only (default true)
        icon: "ic_launcher", // as FCM payload, you can relace this with custom icon you put in mipmap
       
       
        color: "red", // Android only
        vibrate: 300, // Android only default: 300, no vibration if you pass 0
        wake_screen: true, // Android only, wake up screen when notification arrives
        group: "group", // Android only
       
        ongoing: true, // Android only
        my_custom_data: "my_custom_field_value", // extra data you want to throw
        lights: true, // Android only, LED blinking (default false)
        show_in_foreground: val, // notification when app is in foreground (local & remote)
        
        click_action: () => console.log("test"),
    });
    }


    
    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    checkLogin = async () => {
        
        const password = await AsyncStorage.getItem('@password');
        this.pass = password;
        const userJSON = await AsyncStorage.getItem('@BoodyCarUser');
        if(userJSON){
            //this.permissions();
            const userInfo = JSON.parse(userJSON);
            this.props.CheckLoginWhenOpen(userInfo);
            console.log(this.props.currentUser)
            if(userInfo.user.type==='CLIENT'){
                console.log(this.props.currentUser)
                this.props.navigator.resetTo({
                   //screen: 'NotificationSearchedProductResponse',
                    //screen: 'OwnerHome',
                    screen: 'CustomerHome',         
                    animated: true,
                });
            }else{
              if(userInfo.user.active){
                this.props.navigator.resetTo({
                    screen: 'OwnerHome',
                    animated: true,
                });  
              }else{
                  this.props.navigator.resetTo({
                    screen: 'Wait',
                    animated: true,
                    passProps:{password:password}
                });
              }  
            }
        }else{
            //this.permissions();
            this.props.navigator.resetTo({
                screen: 'UserType',
                //screen: 'CustomerHome',  
                animated: true,
            });
        }
        
    }

    checkLanguage = async () => {
        /*Strings.setLanguage('en')
        this.props.changeLanguage(false);*/
        //Strings.setLanguage('e')
       const lang = await AsyncStorage.getItem('@lang');
       //console.log('lang is '+lang)
       if(lang==='ar'){
        this.props.changeLanguage(true);
        Strings.setLanguage('ar')
        }else{
            this.props.changeLanguage(false);
            Strings.setLanguage('en')
        }
        
    }

   
    render(){
        return(
            <ImageBackground style={{flex:1,justifyContent:'center',alignItems:'center'}} source={require('../assets/imgs/boodyCarBackground.png')} >
                <StatusBar hidden />
                <Image style={{width:responsiveWidth(80),height:responsiveHeight(10)}} source={require('../assets/imgs/bluelogo.png')} />
                <View style={{marginTop:moderateScale(5)}}>
                    <AppText text={Strings.logoText} color={colors.primaryColor} fontSize={responsiveFontSize(2.5)} />
                </View>
                <Progress.Bar style={{marginTop:moderateScale(18)}} indeterminate color={colors.primaryColor} width={responsiveWidth(70)} />
                

                <SnackBar
                visible={this.state.showSnack} 
                textMessage={Strings.waitAccept}
                messageColor='white'
                backgroundColor={colors.primaryColor}
                autoHidingTime={5000}
                />
            </ImageBackground>
        );
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    currentUser : state.auth.currentUser,
    userTokens: state.auth.userToken,
})

const mapDispatchToProps = {
    CheckLoginWhenOpen,
    changeLanguage,
    userLocation,
    userToken,
    login
}

export default connect(mapToStateProps,mapDispatchToProps)(SplashScreen);

