import React, { Component } from 'react';
import {
  View,Image,TouchableOpacity,ImageBackground,StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import {Icon,Button} from 'native-base';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import {clientType} from '../actions/SignupAction';
import withPreventDoubleClick from '../components/withPreventDoubleClick';

const MyButton =  withPreventDoubleClick(Button);
const MyTouchableOpacity =  withPreventDoubleClick(TouchableOpacity);

class UserType extends Component {

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };

    componentDidMount(){    
        this.disableDrawer();     
    }


    disableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    

    render(){
        const {isRTL}=this.props
        return(
            <ImageBackground style={{flex:1,justifyContent:'center',alignItems:'center'}} source={require('../assets/imgs/boodyCarBackground.png')} >
                <StatusBar backgroundColor={colors.primaryColor} />
                <View style={{height:responsiveHeight(70), marginTop:moderateScale(20), justifyContent:'center',alignItems:'center'}}>
                    <Image style={{width:responsiveWidth(80),height:responsiveHeight(10)}} source={require('../assets/imgs/bluelogo.png')} />
                    <View style={{marginTop:moderateScale(5)}}>
                        <AppText text={Strings.logoText} color={colors.primaryColor} fontSize={responsiveFontSize(2.5)} />
                    </View>
                    <View style={{marginTop:moderateScale(40)}}>
                        <MyButton
                        onPress={()=>{
                            this.props.clientType('CLIENT')
                            this.props.navigator.push({
                                screen: 'Welcome',
                                animated: true,
                            });
                        }} 
                         style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:colors.primaryColor,borderRadius:moderateScale(2.5)}}>
                            <AppText text={Strings.customer} color='white' fontSize={responsiveFontSize(3)} />
                        </MyButton>

                        <MyButton
                         onPress={()=>{
                            this.props.clientType('OWNER')
                            this.props.navigator.push({
                                screen: 'Welcome',
                                animated: true,
                            });
                        }} 
                         style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(8),width:responsiveWidth(80),backgroundColor:'transparent',borderRadius:moderateScale(2.5),borderColor:colors.primaryColor,borderWidth:moderateScale(.6),marginTop:moderateScale(10)}}>
                            <AppText text={Strings.owner} color={colors.primaryColor} fontSize={responsiveFontSize(3)} />
                        </MyButton>
                    </View>   
                    
                </View>

                <MyTouchableOpacity
                onPress={()=>{
                    this.props.navigator.resetTo({
                        screen: 'CustomerHome',
                        animated: true,
                      });
                }}
                 style={{alignSelf:isRTL?'flex-start':'flex-end',flexDirection:this.props.isRTL? 'row-reverse':'row',marginHorizontal:moderateScale(10),justifyContent:'center',alignItems:"center" }}>
                    <AppText color={colors.skipIconColor} text={Strings.skip} fontSize={responsiveFontSize(3.5)} />
                    <Icon name={isRTL?"chevron-left":"chevron-right"} type="Entypo" style={{color:colors.skipIconColor}} size={responsiveFontSize(2)} />
                </MyTouchableOpacity>

            </ImageBackground>
        );
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
})

const mapDispatchToProps = {
    clientType,
}

export default connect(mapToStateProps,mapDispatchToProps)(UserType);

