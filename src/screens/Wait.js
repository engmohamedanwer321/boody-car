import React,{Component} from 'react';
import {ScrollView, View,Image,RefreshControl,AsyncStorage} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import {login} from '../actions/AuthActions';


class Wait extends Component{

    constructor(props) {
        super(props);
        this.state = {
          refreshing: false,
        };
      }
   

     checkLogin  = () => { 
          console.log(this.props.currentUser.user.phone);
          this.setState({refreshing:true}) 
          axios.post(`${BASE_END_POINT}signin`, JSON.stringify({
                phone:this.props.currentUser.user.phone,
                password:this.props.password
            }), {
            headers: {
              'Content-Type': 'application/json',
            },
          }).then(response => {
            this.setState({refreshing:false}) 
            console.log(response.data);
            AsyncStorage.setItem('@BoodyCarUser', JSON.stringify(response.data));        
            if (response.data.user.type === "OWNER") {
                if (response.data.user.active) {
                    navigator.resetTo({
                      screen: 'OwnerHome',
                      animated: true
                    })
                  }else{
                      console.log('Wait')
                      RNToasty.Info({title:Strings.waitAccept})
                  }
            }
             })
             .catch(error => {
                this.setState({refreshing:false}) 
                 console.log(error.response);
                 if (!error.response) {
                     //console.log()
                 } 
             });

    }

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    
    componentDidMount(){
        this.enableDrawer()
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    render(){
        return(
            <ScrollView 
            refreshControl={
                <RefreshControl
                  refreshing={this.props.loading}
                  onRefresh={()=>{
                    this.props.login(this.props.currentUser.user.phone,this.props.password,this.props.userTokens,this.props.navigator,false)
                  }}
                />
              }
            style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBack title={Strings.wait} />
                 <View style={{marginTop:moderateScale(20),width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
                    <Image
                    resizeMode='contain'
                    source={require('../assets/imgs/wait.png')}
                    style={{width:responsiveWidth(25),height:responsiveHeight(14)}}
                    />
                    <AppText fontSize={responsiveFontSize(3)} text={Strings.requsetInReview} color={colors.primaryColor} />
                    <View style={{width:responsiveWidth(70)}}>
                        <AppText textAlign='center' fontSize={responsiveFontSize(2)} text={Strings.waitAccept} color='#CCCCCC' />
                    </View>
                 </View>

                
                
            </ScrollView>
        )
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser : state.auth.currentUser, 
    userTokens: state.auth.userToken,
    loading: state.auth.loading,
})

const mapDispatchToProps = {
    login
}


export default connect(mapToStateProps,mapDispatchToProps)(Wait);