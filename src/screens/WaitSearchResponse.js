import React,{Component} from 'react';
import {ScrollView, View,Image,RefreshControl,AsyncStorage} from 'react-native';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import { connect } from 'react-redux';
import * as colors from '../assets/colors';
import AppHeader from '../common/AppHeader'
import AppText from '../common/AppText';
import Strings from '../assets/strings';
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import { RNToasty } from 'react-native-toasty';


class WaitSearchResponse extends Component{

    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: colors.darkPrimaryColor,
    };
    
    componentDidMount(){
        this.enableDrawer()
        
    }

    enableDrawer = () => {
        this.props.navigator.setDrawerEnabled({
            side: 'left',
            enabled: false,
        });
        this.props.navigator.setDrawerEnabled({
            side: 'right',
            enabled: false,
        });
    }

    render(){
        return(
            <ScrollView 
            style={{flex:1}}>
                <AppHeader navigator={this.props.navigator} showBack title={Strings.wait} />
                 <View style={{marginTop:moderateScale(20),width:responsiveWidth(100),justifyContent:'center',alignItems:'center'}}>
                    <Image
                    resizeMode='contain'
                    source={require('../assets/imgs/wait.png')}
                    style={{width:responsiveWidth(25),height:responsiveHeight(14)}}
                    />
                    <AppText fontSize={responsiveFontSize(3)} text={Strings.youSearchAboutProduct} color={colors.primaryColor} />
                    <View style={{width:responsiveWidth(70)}}>
                        <AppText textAlign='center' fontSize={responsiveFontSize(2)} text={Strings.waitSearchResponse} color='#CCCCCC' />
                    </View>
                 </View>

                
                
            </ScrollView>
        )
    }
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser : state.auth.currentUser, 
})


export default connect(mapToStateProps)(WaitSearchResponse);